package org.maepaysoh.maepaysohsdk;

/**
 * Created by yemyatthu on 8/11/15.
 */
public class Constants {
  public static final String API_KEY = "d18e6c3eebbd5c538448bf6526e1f4ea5d5ffaf7";
  protected static final String UNICODE = "unicode";
  protected static final String ZAWGYI = "zawgyi";
  protected static final String WITH_PARTY = "party";
  protected static final String SDK_NAME = "maepaysohsdk";
  protected static final String FONT_STORAGE = "font";
  protected static final String SAVED_TOWNSHIP = "township";
}
