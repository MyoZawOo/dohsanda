package org.maepaysoh.maepaysohsdk.utils;

/**
 * Created by arkar on 9/24/15.
 */
public abstract class VoterAPIProperties<T> {
    public static final VoterAPIProperties<String> NRC_NO =
            new VoterAPIProperties<String>(String.class) {
            };
    public static final VoterAPIProperties<String> DOB =
            new VoterAPIProperties<String>(String.class) {
            };
    public static final VoterAPIProperties<String> FATHER_NAME =
            new VoterAPIProperties<String>(String.class) {
            };

    public final Class<T> propertyClass;

    private VoterAPIProperties(Class<T> propertyClass) {
        this.propertyClass = propertyClass;
    }
}
