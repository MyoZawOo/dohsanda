package org.maepaysoh.maepaysohsdk.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by arkar on 9/24/15.
 */
public class VoterAPIPropertiesMap {
    private final Map<VoterAPIProperties<?>, Object> properties =
            new HashMap<VoterAPIProperties<?>, Object>();

    public <T> void put(VoterAPIProperties<T> property, T value) {
        properties.put(property, value);
    }

    public String getString(VoterAPIProperties<String> property, String defaultValue) {
        String result = property.propertyClass.cast(properties.get(property));
        if (result == null) {
            return defaultValue;
        } else {
            return result;
        }
    }

    public boolean getBoolean(VoterAPIProperties<Boolean> property, boolean defaultValue) {
        boolean result;
        try {
            result = property.propertyClass.cast(properties.get(property));
        } catch (Exception e) {
            return defaultValue;
        }
        return result;
    }

    public int getInteger(VoterAPIProperties<Integer> property, int defaultValue) {
        try {
            int result = property.propertyClass.cast(properties.get(property));
            if (result == -1) {
                return defaultValue;
            } else {
                return result;
            }
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public <T> T get(VoterAPIProperties<T> property) {
        return property.propertyClass.cast(properties.get(property));
    }

    public <T> VoterAPIPropertiesMap with(VoterAPIProperties<T> property, T value) {
        put(property, value);
        return this;
    }
}
