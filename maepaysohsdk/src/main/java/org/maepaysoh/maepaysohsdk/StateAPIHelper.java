package org.maepaysoh.maepaysohsdk;

import android.content.Context;

import com.google.gson.Gson;

import org.maepaysoh.maepaysohsdk.models.State;

import java.util.Arrays;
import java.util.List;

/**
 * Created by arkar on 9/21/15.
 */
public class StateAPIHelper {

    private Context mContext;
    private static final String STATE_JSON = "[{\"_id\":\"MMR001\",\"name\":\"ကချင်ပြည်နယ်\"},{\"_id\":\"MMR002\",\"name\":\"ကယားပြည်နယ်\"},{\"_id\":\"MMR003\",\"name\":\"ကရင်ပြည်နယ်\"},{\"_id\":\"MMR004\",\"name\":\"ချင်းပြည်နယ်\"},{\"_id\":\"MMR005\",\"name\":\"စစ်ကိုင်းတိုင်းဒေသကြီး\"},{\"_id\":\"MMR006\",\"name\":\"တင်္နသာရီတိုင်းဒေသကြီး\"},{\"_id\":\"MMR007\",\"name\":\"ပဲခူးတိုင်းဒေသကြီး (အရှေ့)\"},{\"_id\":\"MMR008\",\"name\":\"ပဲခူးတိုင်းဒေသကြီး (အနောက်)\"},{\"_id\":\"MMR009\",\"name\":\"မကွေးတိုင်းဒေသကြီး\"},{\"_id\":\"MMR010\",\"name\":\"မန္တလေးတိုင်းဒေသကြီး\"},{\"_id\":\"MMR011\",\"name\":\"မွန်ပြည်နယ်\"},{\"_id\":\"MMR012\",\"name\":\"ရခိုင်ပြည်နယ်\"},{\"_id\":\"MMR013\",\"name\":\"ရန်ကုန်တိုင်းဒေသကြီး\"},{\"_id\":\"MMR014\",\"name\":\"ရှမ်းပြည်နယ် (တောင်)\"},{\"_id\":\"MMR015\",\"name\":\"ရှမ်းပြည်နယ် (မြောက်)\"},{\"_id\":\"MMR016\",\"name\":\"ရှမ်းပြည်နယ် (အရှေ့)\"},{\"_id\":\"MMR017\",\"name\":\"ဧရာဝတီတိုင်းဒေသကြီး\"},{\"_id\":\"MMR018\",\"name\":\"နေပြည်တော်\"}]";

    protected StateAPIHelper(Context context) {
        mContext = context;
    }

    public List<State> getAllStatesData() {
        State[] stateArray = new Gson().fromJson(STATE_JSON, State[].class);
        return Arrays.asList(stateArray);
    }

    public State getStateById(String id) {
        State[] stateArray = new Gson().fromJson(STATE_JSON, State[].class);
        int len = stateArray.length;
        for (int i = 0; i < len; i++) {
            State state = stateArray[i];
            if (state.getId().equals(id)) {
                return state;
            }
        }
        return null;
    }
}
