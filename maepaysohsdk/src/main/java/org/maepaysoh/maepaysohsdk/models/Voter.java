package org.maepaysoh.maepaysohsdk.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by arkar on 9/24/15.
 */
public class Voter {
    @SerializedName("dateofbirth") private String dateOfBirth;
    private String village;
    @SerializedName("father_name") private String fatherName;
    private String nrcno;
    private String state;
    @SerializedName("voter_name") private String voterName;
    @SerializedName("dateofbirth_num") private String dateofbirthNum;
    @SerializedName("mother_name") private String motherName;
    private String township;
    private String district;
    private List<String> ethnics;
    private int code;
    private String message;

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getNrcno() {
        return nrcno;
    }

    public void setNrcno(String nrcno) {
        this.nrcno = nrcno;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVoterName() {
        return voterName;
    }

    public void setVoterName(String voterName) {
        this.voterName = voterName;
    }

    public String getDateofbirthNum() {
        return dateofbirthNum;
    }

    public void setDateofbirthNum(String dateofbirthNum) {
        this.dateofbirthNum = dateofbirthNum;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public List<String> getEthnics() {
        return ethnics;
    }

    public void setEthnics(List<String> ethnics) {
        this.ethnics = ethnics;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
