package org.maepaysoh.maepaysohsdk.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ye Lin Aung on 15/08/03.
 */
public class Candidate implements Serializable {

  private String id;
  private String name;
  private String legislature;
  @SerializedName("photo_url") private String photoUrl;
  @SerializedName("national_id") private String nationalId;
  private int birthdate;
  private String education;
  private String occupation;
  @SerializedName("religion") private String religion;
  private String ethnicity;
  @SerializedName("ward_village") private String wardVillage;
  private Constituency constituency;
  @SerializedName("party_id") private String partyId;
  private Mother mother;
  private Father father;
  private String gender;
  private Party party;
  private String partyName;
  private String mpid;

  public Candidate() {
  }

  /**
   * @return The id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return The name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name The name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return The legislature
   */
  public String getLegislature() {
    return legislature;
  }

  /**
   * @param legislature The legislature
   */
  public void setLegislature(String legislature) {
    this.legislature = legislature;
  }

  /**
   * @return The photoUrl
   */
  public String getPhotoUrl() {
    return photoUrl;
  }

  /**
   * @param photoUrl The photourl
   */
  public void setPhotoUrl(String photoUrl) {
    this.photoUrl = photoUrl;
  }

  /**
   * @return The nationalId
   */
  public String getNationalId() {
    return nationalId;
  }

  /**
   * @param nationalId The national_id
   */
  public void setNationalId(String nationalId) {
    this.nationalId = nationalId;
  }

  /**
   * @return The birthdate
   */
  public int getBirthdate() {
    return birthdate;
  }

  /**
   * @param birthdate The birthdate
   */
  public void setBirthdate(int birthdate) {
    this.birthdate = birthdate;
  }

  /**
   * @return The education
   */
  public String getEducation() {
    return education;
  }

  /**
   * @param education The education
   */
  public void setEducation(String education) {
    this.education = education;
  }

  /**
   * @return The occupation
   */
  public String getOccupation() {
    return occupation;
  }

  /**
   * @param occupation The occupation
   */
  public void setOccupation(String occupation) {
    this.occupation = occupation;
  }

  /**
   * @return The religion
   */
  public String getReligion() {
    return religion;
  }

  /**
   * @param religion The religion
   */
  public void setReligion(String religion) {
    this.religion = religion;
  }

  /**
   * @return The wardVillage
   */
  public String  getWardVillage() {
    return wardVillage;
  }

  /**
   * @param wardVillage The wardVillage
   */
  public void setWardVillage(String wardVillage) {
    this.wardVillage = wardVillage;
  }

  /**
   * @return The constituency
   */
  public Constituency getConstituency() {
    return constituency;
  }

  /**
   * @param constituency The constituency
   */
  public void setConstituency(Constituency constituency) {
    this.constituency = constituency;
  }

  /**
   * @return The partyId
   */
  public String getPartyId() {
    return partyId;
  }

  /**
   * @param partyId The party_id
   */
  public void setPartyId(String partyId) {
    this.partyId = partyId;
  }

  /**
   * @return The mother
   */
  public Mother getMother() {
    return mother;
  }

  /**
   * @param mother The mother
   */
  public void setMother(Mother mother) {
    this.mother = mother;
  }

  /**
   * @return The father
   */
  public Father getFather() {
    return father;
  }

  /**
   * @param father The father
   */
  public void setFather(Father father) {
    this.father = father;
  }

  /**
   * @return The gender
   */
  public String getGender() {
    return gender;
  }

  /**
   * @param gender The father
   */
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * @return The party
   */
  public Party getParty() {
    return party;
  }

  /**
   * @param party The father
   */
  public void setParty(Party party) {
    this.party = party;
  }

  /**
   * @return The partyName
   */
  public String getPartyName() {
    return partyName;
  }

  /**
   * @param partyName The father
   */
  public void setPartyName(String partyName) {
    this.partyName = partyName;
  }

  /**
   * @return The mpid
   */
  public String getMpid() {
    return mpid;
  }

  /**
   * @param mpid The father
   */
  public void setMpid(String mpid) {
    this.mpid = mpid;
  }

  /**
   * @return The ethnicity
   */
  public String getEthnicity() {
    return ethnicity;
  }

  /**
   * @param ethnicity The ethnicity
   */
  public void setEthnicity(String ethnicity) {
    this.ethnicity = ethnicity;
  }
}
