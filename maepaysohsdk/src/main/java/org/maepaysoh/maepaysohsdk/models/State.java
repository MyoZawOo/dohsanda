package org.maepaysoh.maepaysohsdk.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by arkar on 9/20/15.
 */
public class State {
    @SerializedName("_id") private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
