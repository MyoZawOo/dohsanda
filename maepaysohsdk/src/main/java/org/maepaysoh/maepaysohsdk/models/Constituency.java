package org.maepaysoh.maepaysohsdk.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ye Lin Aung on 15/08/03.
 */
public class Constituency implements Serializable {
  private String type;
  private String name;
  private int count;
  private String parent;
  @SerializedName("ST_PCODE") private String sTPCode;
  @SerializedName("DT_PCODE") private String dTPCode;
  @SerializedName("TS_PCODE") private String tSPCode;
  @SerializedName("AM_PCODE") private String aMPCode;

  public Constituency() {
  }

  /**
   * @return The type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type The type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return The name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name The name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return The count
   */
  public int getCount() {
    return count;
  }

  /**
   * @param count The count
   */
  public void setCount(int count) {
    this.count = count;
  }

  /**
   * @return The parent
   */
  public String getParent() {
    return parent;
  }

  /**
   * @param parent The parent
   */
  public void setParent(String parent) {
    this.parent = parent;
  }

  /**
   * @return The ST_PCODE
   */
  public String getsTPCode() {
    return sTPCode;
  }

  /**
   * @param sTPCode The ST_PCODE
   */
  public void setsTPCode(String sTPCode) {
    this.sTPCode = sTPCode;
  }

  /**
   * @return The DT_PCODE
   */
  public String getdTPCode() {
    return dTPCode;
  }

  /**
   * @param dTPCode The DT_PCODE
   */
  public void setdTPCode(String dTPCode) {
    this.dTPCode = dTPCode;
  }

  /**
   * @return The TS_PCODE
   */
  public String gettSPCode() {
    return tSPCode;
  }

  /**
   * @param tSPCode The TS_PCODE
   */
  public void settSPCode(String tSPCode) {
    this.tSPCode = tSPCode;
  }

  /**
   * @return The AM_PCODE
   */
  public String getaMPCode() {
    return aMPCode;
  }

  /**
   * @param aMPCode The TS_PCODE
   */
  public void setaMPCode(String aMPCode) {
    this.aMPCode = aMPCode;
  }
}
