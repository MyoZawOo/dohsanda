package org.maepaysoh.maepaysohsdk.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import org.maepaysoh.maepaysohsdk.models.Candidate;
import org.maepaysohsdk.maepaysohsdk.BuildConfig;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by yemyatthu on 8/4/15.
 */
public class RetrofitHelper {

  public static RestAdapter getResAdapter(final String token) {
    Gson gson = new GsonBuilder()
          .registerTypeAdapter(Candidate.class, new CandidateDeserializer())
          .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
          .create();

    if (BuildConfig.DEBUG) {
      return new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()))
          .setConverter(new GsonConverter(gson))
          .setEndpoint("http://api.maepaysoh.org")
          .setLogLevel(RestAdapter.LogLevel.FULL)
          .setRequestInterceptor(new RequestInterceptor() {
            @Override public void intercept(RequestFacade request) {
              request.addQueryParam("token", token);
            }
          })
          .build();
    } else {
      return new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()))
          .setEndpoint("http://api.maepaysoh.org")
          .setConverter(new GsonConverter(gson))
          .setLogLevel(RestAdapter.LogLevel.NONE)
          .setRequestInterceptor(new RequestInterceptor() {
            @Override public void intercept(RequestFacade request) {
              request.addQueryParam("token", token);
            }
          })
          .build();
    }
  }

  public static RestAdapter getPublicResAdapter() {
    if (BuildConfig.DEBUG) {
      return new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()))
          .setEndpoint("http://api.maepaysoh.org")
          .setLogLevel(RestAdapter.LogLevel.FULL)
          .build();
    } else {
      return new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()))
          .setEndpoint("http://api.maepaysoh.org")
          .setLogLevel(RestAdapter.LogLevel.NONE)
          .build();
    }
  }

  public static RestAdapter getVoterListResAdapter() {
    Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

    if (BuildConfig.DEBUG) {
      return new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()))
                .setConverter(new GsonConverter(gson))
                .setEndpoint("https://checkvoterlist.uecmyanmar.org")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    } else {
      return new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()))
                .setEndpoint("https://checkvoterlist.uecmyanmar.org")
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .build();
    }
  }
}
