package org.maepaysoh.maepaysohsdk.api;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.maepaysoh.maepaysohsdk.models.Candidate;
import org.maepaysoh.maepaysohsdk.models.Party;

import java.lang.reflect.Type;

/**
 * Created by arkar on 9/17/15.
 */
public class CandidateDeserializer implements JsonDeserializer<Candidate> {
    @Override
    public Candidate deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {

        // Get the "content" element from the parsed JSON
        JsonObject jsonObject = je.getAsJsonObject();

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer

        if (jsonObject.has("party") && jsonObject.get("party").isJsonArray())
        {
            Gson gson = new Gson();
            Party party = new Party();
            jsonObject.remove("party");
            jsonObject.add("party", gson.toJsonTree(party));
            je = gson.toJsonTree(jsonObject);
        }

        return new Gson().fromJson(je, Candidate.class);

    }
}
