package org.maepaysoh.maepaysohsdk.api;

import org.maepaysoh.maepaysohsdk.models.Voter;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * Created by arkar on 9/24/15.
 */
public interface VoterService {
    @GET("/api")
    Voter searchVoter(
            @Query("voter_name") String voterName,
            @QueryMap Map<PARAM_FIELD, String> options
    );

    @GET("/api")
    void searchVoterAsync(
            @Query("voter_name") String voterName,
            @QueryMap Map<PARAM_FIELD, String> options,
            Callback<Voter> callback
    );

    enum PARAM_FIELD {
        dateofbirth,
        father_name,
        nrcno
    }
}
