package org.maepaysoh.maepaysohsdk.api;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by arkar on 9/25/15.
 */
public abstract class CancelableCallback<T> implements Callback<T> {

    private boolean canceled;
    private T pendingT;
    private Response pendingResponse;
    private RetrofitError pendingError;

    public CancelableCallback() {
        this.canceled = false;
    }

    public void cancel(boolean remove) {
        canceled = true;
    }

    @Override
    public void success(T t, Response response) {
        if (canceled) {
            return;
        }
        onSuccess(t, response);
    }

    @Override
    public void failure(RetrofitError error) {
        if (canceled) {
            return;
        }
        onFailure(error);
    }

    protected abstract void onSuccess(T t, Response response);

    protected abstract void onFailure(RetrofitError error);
}
