package org.maepaysoh.maepaysohsdk.api;

import org.maepaysoh.maepaysohsdk.models.PartyListReturnObject;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Created by Ye Lin Aung on 15/08/04.
 */
public interface PartyService {
  @GET("/party") void listPartiesAsync(@QueryMap Map<PARAM_FIELD, String> options,
      Callback<PartyListReturnObject> partyCallback);

  @GET("/party") PartyListReturnObject listParties(@QueryMap Map<PARAM_FIELD, String> options);

  enum PARAM_FIELD {
    font,
    per_page,
    page,
  }
}
