package org.maepaysoh.maepaysohsdk.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import org.maepaysoh.maepaysohsdk.models.Township;

import java.util.ArrayList;
import java.util.List;

import static org.maepaysoh.maepaysohsdk.utils.Logger.LOGE;
import static org.maepaysoh.maepaysohsdk.utils.Logger.makeLogTag;

/**
 * Created by arkar on 9/20/15.
 */
public class TownshipDao {
    private SQLiteDatabase mMaepaysohDb;
    private MaepaysohDbHelper mMaepaysohDbHelper;

    private static final String TAG = makeLogTag(FaqDao.class);

    public TownshipDao(Context context) {
        mMaepaysohDbHelper = new MaepaysohDbHelper(context);
    }

    public void open() throws SQLException {
        mMaepaysohDb = mMaepaysohDbHelper.getWritableDatabase();
    }

    public void close() throws SQLException {
        mMaepaysohDbHelper.close();
    }

    public boolean createTownship(Township township) throws SQLException {
        open();
        ContentValues townshipContentValues = new ContentValues();
        townshipContentValues.put(MaepaysohDbHelper.COLUMN_TOWNSHIP_ID, township.getId());
        townshipContentValues.put(MaepaysohDbHelper.COLUMN_TOWNSHIP_NAME, township.getName());
        townshipContentValues.put(MaepaysohDbHelper.COLUMN_TOWNSHIP_STATE_CODE, township.getStateCode());
        mMaepaysohDb.beginTransaction();
        try {
            long insertId = mMaepaysohDb.insertWithOnConflict(MaepaysohDbHelper.TABLE_NAME_TOWNSHIP,
                    null, townshipContentValues, SQLiteDatabase.CONFLICT_REPLACE);
            mMaepaysohDb.setTransactionSuccessful();
        } catch (SQLiteException e) {
            LOGE(TAG, e.getLocalizedMessage());
        } finally {
            mMaepaysohDb.endTransaction();
        }
        return true;
    }

    public List<Township> getAllTownshipData() throws SQLException {
        open();
        List<Township> townships = new ArrayList<>();
        Cursor cursor =
                mMaepaysohDb.query(MaepaysohDbHelper.TABLE_NAME_TOWNSHIP, null, null, null, null,
                        null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Township township = cursorToTownship(cursor);
            townships.add(township);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return townships;
    }

    public List<Township> getTownshipByState(String stateCode) throws SQLException {
        open();
        List<Township> townships = new ArrayList<>();
        Cursor cursor = mMaepaysohDb.query(MaepaysohDbHelper.TABLE_NAME_TOWNSHIP, null,
                MaepaysohDbHelper.COLUMN_TOWNSHIP_STATE_CODE + " = " + '"' + stateCode + '"',
                null, null, null, MaepaysohDbHelper.COLUMN_TOWNSHIP_NAME);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Township township = cursorToTownship(cursor);
            townships.add(township);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return townships;
    }

    private Township cursorToTownship(Cursor cursor) {
        Township township = new Township();
        township.setId(cursor.getString(cursor.getColumnIndexOrThrow(
                MaepaysohDbHelper.COLUMN_TOWNSHIP_ID)));
        township.setName(
                cursor.getString(cursor.getColumnIndexOrThrow(
                        MaepaysohDbHelper.COLUMN_TOWNSHIP_NAME)));
        township.setStateCode(
                cursor.getString(cursor.getColumnIndexOrThrow(
                        MaepaysohDbHelper.COLUMN_TOWNSHIP_STATE_CODE)));
        return township;
    }
}
