package org.maepaysoh.maepaysohsdk;

import android.content.Context;

import org.maepaysoh.maepaysohsdk.api.RetrofitHelper;
import org.maepaysoh.maepaysohsdk.api.VoterService;
import org.maepaysoh.maepaysohsdk.models.Voter;
import org.maepaysoh.maepaysohsdk.utils.VoterAPIProperties;
import org.maepaysoh.maepaysohsdk.utils.VoterAPIPropertiesMap;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;

/**
 * Created by arkar on 9/24/15.
 */
public class VoterAPIHelper {
    private RestAdapter mVoterListRestAdapter;
    private VoterService mVoterService;
    private Context mContext;

    protected VoterAPIHelper(Context context) {
        mVoterListRestAdapter = RetrofitHelper.getVoterListResAdapter();
        mVoterService = mVoterListRestAdapter.create(VoterService.class);
        mContext = context;
    }

    public Voter searchVoter(String name, VoterAPIPropertiesMap propertiesMap) {
        String nrcNo = propertiesMap.getString(VoterAPIProperties.NRC_NO, "");
        String dob = propertiesMap.getString(VoterAPIProperties.DOB, "");

        Map<VoterService.PARAM_FIELD, String> optionParams = new HashMap<>();
        if (! nrcNo.isEmpty()) {
            optionParams.put(VoterService.PARAM_FIELD.nrcno, nrcNo);
        }

        if (! dob.isEmpty()) {
            optionParams.put(VoterService.PARAM_FIELD.dateofbirth, dob);
        }

        return mVoterService.searchVoter(name, optionParams);
    }

    public void searchVoter(String name, VoterAPIPropertiesMap propertiesMap,
                             Callback<Voter> callback) {
        String nrcNo = propertiesMap.getString(VoterAPIProperties.NRC_NO, "");
        String dob = propertiesMap.getString(VoterAPIProperties.DOB, "");
        String fatherName = propertiesMap.getString(VoterAPIProperties.FATHER_NAME, "");
        Map<VoterService.PARAM_FIELD, String> optionParams = new HashMap<>();
        if (! nrcNo.isEmpty()) {
            optionParams.put(VoterService.PARAM_FIELD.nrcno, nrcNo);
        }

        if (! dob.isEmpty()) {
            optionParams.put(VoterService.PARAM_FIELD.dateofbirth, dob);
        }

        if (! fatherName.isEmpty()) {
            optionParams.put(VoterService.PARAM_FIELD.father_name, fatherName);
        }

        mVoterService.searchVoterAsync(name, optionParams, callback);
    }
}
