package com.dohsanda.education;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.dohsanda.dohsanda.R;

/**
 * Created by myozawoo on 9/24/15.
 */
public class BallotViewPagerAdapter extends PagerAdapter {

    Context mContext;
    int[] ballot;
    LayoutInflater inflater;

    public BallotViewPagerAdapter(Context context, int[] ballot) {
        this.mContext = context;
        this.ballot = ballot;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public int getCount() {
        return ballot.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = inflater.inflate(R.layout.item_ballot, container, false);
        ImageView imgBallot = (ImageView) itemView.findViewById(R.id.img_valid);
        ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.ballot_progress_bar);

        progressBar.getIndeterminateDrawable()
                .setColorFilter(mContext.getResources()
                        .getColor(R.color.primary), PorterDuff.Mode.SRC_ATOP);

        Glide.with(mContext)
                .load(ballot[position])
                .into(imgBallot);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
