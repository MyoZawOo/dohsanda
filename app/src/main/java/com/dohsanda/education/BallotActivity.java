package com.dohsanda.education;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dohsanda.dohsanda.R;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by myozawoo on 9/24/15.
 */
public class BallotActivity extends AppCompatActivity {

    public static final String EXTRA_BALLOT_TYPE = "com.dohsanda.education.ValidBallotActivity.EXTRA_BALLOT_TYPE";

    private static final int[] DESC = new int[]{
            R.string.invalid_one, R.string.invalid_two, R.string.invalid_three, R.string.invalid_four,
            R.string.invalid_five, R.string.invalid_six, R.string.invalid_seven, R.string.invalid_eight,
            R.string.valid, R.string.valid, R.string.valid, R.string.valid,
            R.string.valid, R.string.valid, R.string.valid, R.string.valid
    };


    private ViewPager mViewPager;
    private int currentPosition = 0;
    private ImageButton mBtnNext;
    private ImageButton mBtnPrev;
    private TextView mTxtDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ballot);

        mBtnNext = (ImageButton) findViewById(R.id.btnNext);
        mBtnPrev = (ImageButton) findViewById(R.id.btnBack);
        mTxtDesc = (TextView) findViewById(R.id.txtDesc);
        mViewPager = (ViewPager) findViewById(R.id.ballot_pager);


        final int[] ballot = new int[]{R.drawable.invalid_one, R.drawable.invalid_two, R.drawable.invalid_three, R.drawable.invalid_four,
                R.drawable.invalid_five, R.drawable.invalid_six, R.drawable.invalid_seven, R.drawable.invalid_eight, R.drawable.valid_one, R.drawable.valid_two, R.drawable.valid_three, R.drawable.valid_four,
                R.drawable.valid_five, R.drawable.valid_six, R.drawable.valid_seven, R.drawable.valid_eight};

        mTxtDesc.setText(mmtext.processText(getString(DESC[0]),
                mmtext.TEXT_UNICODE, true, true));
        setBallotPager(ballot);


        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(currentPosition + 1, true);
            }
        });

        mBtnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(currentPosition - 1, true);
            }
        });

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
    }

    private void setBallotPager(final int[] data) {
        PagerAdapter adapter = new BallotViewPagerAdapter(this, data);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;

                mTxtDesc.setText(mmtext.processText(getString(DESC[position]),
                        mmtext.TEXT_UNICODE, true, true));


                if (position == 0) {
                    mBtnPrev.setVisibility(View.INVISIBLE);
                } else {
                    mBtnPrev.setVisibility(View.VISIBLE);
                }

                if (position == data.length - 1) {
                    mBtnNext.setVisibility(View.INVISIBLE);
                } else {
                    mBtnNext.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
