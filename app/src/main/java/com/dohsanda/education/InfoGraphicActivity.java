package com.dohsanda.education;

/**
 * Created by myozawoo on 9/14/15.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.dohsanda.dohsanda.R;
import com.nineoldandroids.view.ViewHelper;

import mm.technomation.tmmtextutilities.mmtext;


public class InfoGraphicActivity extends AppCompatActivity {

    static final int NUM_PAGES = 9;

    ViewPager pager;
    PagerAdapter pagerAdapter;
    LinearLayout circles;
    ImageButton skip;
    Button done;
    ImageButton next;
    boolean isOpaque = true;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.infographic_activity);

        skip = ImageButton.class.cast(findViewById(R.id.skip));
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pager.setCurrentItem(pager.getCurrentItem() - 1, true);
            }
        });
        skip.setVisibility(View.GONE);

        next = ImageButton.class.cast(findViewById(R.id.next));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() + 1, true);
            }
        });

        done = Button.class.cast(findViewById(R.id.done));
        done.setText("ထွက်မည်");
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTutorial();
            }
        });

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setPageTransformer(true, new CrossfadePageTransformer());
        //pager.setCurrentItem(2);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == NUM_PAGES - 2 && positionOffset > 0) {
                    if (isOpaque) {
                        pager.setBackgroundColor(Color.TRANSPARENT);
                        isOpaque = false;
                    }
                } else {
                    if (!isOpaque) {
                        pager.setBackgroundColor(getResources().getColor(R.color.primary_material_light));
                        isOpaque = true;
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
                if (position == 0) {
                    skip.setVisibility(View.GONE);
                } else if (position == NUM_PAGES - 2) {
                    skip.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                    done.setVisibility(View.VISIBLE);
                } else if (position < NUM_PAGES - 2) {
                    skip.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);
                    done.setVisibility(View.GONE);
                } else if (position == NUM_PAGES - 1) {
                    endTutorial();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        buildCircles();

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true ,true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(pager!=null){
            pager.clearOnPageChangeListeners();
        }
    }

    private void buildCircles(){
        circles = LinearLayout.class.cast(findViewById(R.id.circles));

        float scale = getResources().getDisplayMetrics().density;
        int padding = (int) (5 * scale + 0.5f);

        for(int i = 0 ; i < NUM_PAGES - 1 ; i++){

            ImageView circle = new ImageView(this);

            Bitmap tmpBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.inactive_stick);
            Bitmap circleBitmap = Bitmap.createScaledBitmap(tmpBitmap, (int) scale * 20, (int) scale * 2, false);
            //circle.setImageResource(R.drawable.ic_swipe_indicator_white_18dp);
            circle.setImageBitmap(circleBitmap);
           // circle.setColorFilter(getResources().getColor(R.color.primary));
            circle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            circle.setAdjustViewBounds(true);
            //circle.setPadding(padding, 0, padding, 0);
            circles.addView(circle);
        }
        setIndicator(0);
    }

    private void setIndicator(int index){
        if(index < NUM_PAGES){
            for(int i = 0 ; i < NUM_PAGES - 1 ; i++){
                ImageView circle = (ImageView) circles.getChildAt(i);
                if(i == index){
                    float scale = getResources().getDisplayMetrics().density;
                    Bitmap tmpBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.whtie_stick);
                    Bitmap whiteStick = Bitmap.createScaledBitmap(tmpBitmap, (int)scale*20, (int)scale*2, false);
                    circle.setImageBitmap(whiteStick);

                    //circle.setColorFilter(getResources().getColor(R.color.text_selected));
                }else {
                    float scale = getResources().getDisplayMetrics().density;
                    Bitmap tmpBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.inactive_stick);
                    Bitmap circleBitmap = Bitmap.createScaledBitmap(tmpBitmap, (int)scale*20, (int)scale*2, false);
                    circle.setImageBitmap(circleBitmap);
                    //circle.setColorFilter(getResources().getColor(android.R.color.transparent));
                }
            }
        }
    }

    private void endTutorial(){
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onBackPressed() {
        endTutorial();
    }

    private class ScreenSlidePagerAdapter extends  FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ViewPagerFragment tp = null;
            switch(position){
                case 0:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment01);

                    break;
                case 1:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment02);
                    break;
                case 2:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment03);
                    break;
                case 3:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment04);
                    break;
                case 4:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment05);
                    break;
                case 5:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment06);
                    break;
                case 6:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment07);
                    break;
                case 7:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment08);
                    break;
                case 8:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment5);
                    break;
            }

            return tp;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class CrossfadePageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View page, float position) {
            int pageWidth = page.getWidth();

            View backgroundView = page.findViewById(R.id.welcome_fragment);
            View text_head= page.findViewById(R.id.heading);
            View text_content = page.findViewById(R.id.txt_explanation);
            final ScrollView scroll = (ScrollView) page.findViewById(R.id.scroll);

            if(0 <= position && position < 1){
                ViewHelper.setTranslationX(page, pageWidth * -position);
            }
            if(-1 < position && position < 0){
                ViewHelper.setTranslationX(page, pageWidth * -position);
            }

            if(position <= -1.0f || position >= 1.0f) {

            } else if ( position == 0.0f ) {

            } else {
                if(backgroundView != null) {
                    ViewHelper.setAlpha(backgroundView, 1.0f - Math.abs(position));
                }

                if (text_head != null) {
                    ViewHelper.setTranslationX(text_head,pageWidth * position);
                    ViewHelper.setAlpha(text_head, 1.0f - Math.abs(position));
                }

                if (text_content != null) {
                    ViewHelper.setTranslationX(text_content,pageWidth * position);
                    ViewHelper.setAlpha(text_content, 1.0f - Math.abs(position));
                }

                if (scroll!= null) {
                    ViewHelper.setTranslationX(scroll, pageWidth * position);
                    ViewHelper.setAlpha(scroll, 1.0f - Math.abs(position));
                    scroll.setPadding(0, 0, 0, 0);
                }
            }
        }
    }


}
