package com.dohsanda.education;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dohsanda.dohsanda.R;



/**
 * Created by myozawoo on 10/5/15.
 */
public class InstructionActivity extends AppCompatActivity{

    private ViewPager mViewPager;
    private int currentPosition = 0;
    private ImageButton mBtnNext;
    private ImageButton mBtnPrev;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        mBtnNext = (ImageButton) findViewById(R.id.btnNext);
        mBtnPrev = (ImageButton) findViewById(R.id.btnBack);

        mViewPager = (ViewPager) findViewById(R.id.ballot_pager);


        final int[] instruction = new int[]{R.drawable.instruction_title, R.drawable.instruction_one, R.drawable.instruction_two,
                R.drawable.instruction_three, R.drawable.instruction_four, R.drawable.instruction_five, R.drawable.instruction_six, R.drawable.instruction_seven };


        setBallotPager(instruction);


        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(currentPosition + 1, true);
            }
        });

        mBtnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(currentPosition - 1, true);
            }
        });


    }

    private void setBallotPager(final int[] data) {
        PagerAdapter adapter = new BallotViewPagerAdapter(this, data);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;



                if (position == 0) {
                    mBtnPrev.setVisibility(View.INVISIBLE);
                } else {
                    mBtnPrev.setVisibility(View.VISIBLE);
                }

                if (position == data.length - 1) {
                    mBtnNext.setVisibility(View.INVISIBLE);
                } else {
                    mBtnNext.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }}
