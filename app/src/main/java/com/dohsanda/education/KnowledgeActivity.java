package com.dohsanda.education;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dohsanda.dohsanda.AboutActivity;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.ui.BaseActivity;
import com.dohsanda.dohsanda.utils.TimeUtils;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by myozawoo on 9/23/15.
 */
public class KnowledgeActivity extends BaseActivity {

    private ImageView btnEditorial, btnFaq, btnInstruction, btnBallot;
    private ImageView imgCandidate, imgParty, imgGame;
    private TextView txtTips;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge);

        btnEditorial = (ImageView) findViewById(R.id.btnEditorial);
        btnFaq = (ImageView) findViewById(R.id.btnFaq);
        btnInstruction = (ImageView) findViewById(R.id.btnInstruction);
        btnBallot = (ImageView) findViewById(R.id.btnBallot);


        txtTips = (TextView) findViewById(R.id.txtTips);

        imgCandidate = (ImageView) findViewById(R.id.imgCandidate);
        imgParty = (ImageView) findViewById(R.id.imgParty);
        imgGame = (ImageView) findViewById(R.id.imgGame);


        mToolbar = (Toolbar) findViewById(R.id.home_toolbar);
        mToolbar.setTitle(getString(R.string.KnowledgeTitle));
        mToolbar.setLogo(R.drawable.home_icon_drawable);



        mToolbar = (Toolbar) findViewById(R.id.home_toolbar);
        mToolbar.setTitle(getString(R.string.KnowledgeTitle));
        //mToolbar.setBackgroundColor(getResources().getColor(R.color.knowledge));


        setSupportActionBar(mToolbar);

        imgCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });

        imgParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });

        imgGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });


        imgCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });

        imgParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });

        imgGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });


        btnEditorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KnowledgeActivity.this, InfoGraphicActivity.class));
            }
        });

        btnFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KnowledgeActivity.this, FAQRecyclerActivity.class));
            }
        });

        btnInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(KnowledgeActivity.this, InstructionActivity.class));
            }
        });



        btnBallot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KnowledgeActivity.this, BallotActivity.class);
                startActivity(intent);
            }
        });

        int days = new TimeUtils().daysCount();
        if (days > 0) {
            randomTips();
        }

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true ,true);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.license:
                showLicenseDialog();
                return true;

            case R.id.about:
                goToAbout();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void randomTips() {

        final int[] tips = {R.string.tip_one, R.string.tip_two, R.string.tip_three, R.string.tip_four,
                R.string.tip_five, R.string.tip_six};

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                Random r = new Random();

                int num = r.nextInt(5);
                txtTips.setText(tips[num]);
                mmtext.prepareView(getBaseContext(), txtTips, mmtext.TEXT_UNICODE, true, true);
            }
        };

        Timer tipChangeTimer = new Timer();
        tipChangeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 400, 5000);
    }


    private void  showLicenseDialog() {
        View licenseView = LayoutInflater.from(this).inflate(R.layout.license_dialog, null);
        TextView aboutBodyView = (TextView) licenseView.findViewById(R.id.about_body_extras);
        aboutBodyView.setText(Html.fromHtml(getString(R.string.license_body_extras)));
        aboutBodyView.setMovementMethod(new LinkMovementMethod());
        new AlertDialog.Builder(this)
                .setView(licenseView)
                .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void goToAbout() {
        startActivity(new Intent(KnowledgeActivity.this, AboutActivity.class));
    }


}
