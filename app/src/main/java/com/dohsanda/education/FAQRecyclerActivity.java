package com.dohsanda.education;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.model.Answer;
import com.dohsanda.dohsanda.model.Question;
import com.dohsanda.dohsanda.ui.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;


/**
 * Created by myozawoo on 9/19/15.
 */
public class FAQRecyclerActivity extends BaseActivity {

    private Toolbar mToolbar;

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, FAQRecyclerActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_recycler_view_activity);

        View mToolbarShadow = findViewById(R.id.faq_list_toolbar_shadow);
        mToolbar = (Toolbar) findViewById(R.id.faq_toolbar);
        //hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);
        mToolbar.setTitle(getString(R.string.FAQTitle));
        mToolbar.setBackgroundColor(getResources().getColor(R.color.knowledge));
        setStatusBarColor(R.color.dark_knowledge);

        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();

        if (mActionBar != null) {
            // Showing Back Arrow  <-
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
        setupFAQ();
        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
    }

    private void setupFAQ() {
        int[] QUESTIONS = {
                R.string.question_1, R.string.question_2, R.string.question_3,
                R.string.question_4, R.string.question_5, R.string.question_6, R.string.question_7,
                R.string.question_8, R.string.question_9, R.string.question_11,
                R.string.question_12, R.string.question_13,  R.string.question_18, R.string.question_14,
                R.string.question_16, R.string.question_17, R.string.question_15, R.string.question_19,
                R.string.question_20, R.string.question_21, R.string.question_22, R.string.question_23,
                R.string.question_24, R.string.question_25, R.string.question_26, R.string.question_27,
                R.string.question_28, R.string.question_29, R.string.question_30, R.string.question_31,
                R.string.question_32, R.string.question_33, R.string.question_34, R.string.question_35,
                R.string.question_36, R.string.question_37, R.string.question_38, R.string.question_39,
                R.string.question_40, R.string.question_41, R.string.question_42, R.string.question_43,
                R.string.question_44, R.string.question_45, R.string.question_46, R.string.question_47,
        };

        int[] ANSWERS = {
                R.string.answer_1, R.string.answer_2, R.string.answer_3,
                R.string.answer_4, R.string.answer_5, R.string.answer_6, R.string.answer_7,
                R.string.answer_8, R.string.answer_9, R.string.answer_11,
                R.string.answer_12, R.string.answer_13, R.string.answer_18, R.string.answer_14,
                R.string.answer_16, R.string.answer_17, R.string.answer_15, R.string.answer_19,
                R.string.answer_20, R.string.answer_21, R.string.answer_22, R.string.answer_23,
                R.string.answer_24, R.string.answer_25, R.string.answer_26, R.string.answer_27,
                R.string.answer_28, R.string.answer_29, R.string.answer_30, R.string.answer_31,
                R.string.answer_32, R.string.answer_33, R.string.answer_34, R.string.answer_35,
                R.string.answer_36, R.string.answer_37, R.string.answer_38, R.string.answer_39,
                R.string.answer_40, R.string.answer_41, R.string.answer_42, R.string.answer_43,
                R.string.answer_44, R.string.answer_45, R.string.answer_46, R.string.answer_47,
        };

        int questionSize = QUESTIONS.length;
        List<Question> questionList = new ArrayList<>();
        for (int i = 0; i<  questionSize; i++) {
            List<Answer> answerList = new ArrayList<>();
            Question question = new Question(getString(QUESTIONS[i]));
            answerList.add(new Answer(getString(ANSWERS[i])));
            question.setChildItemList(answerList);
            questionList.add(question);
        }


        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FAQRecyclerAdapter adapter = new FAQRecyclerAdapter(questionList);
        adapter.setContext(this);
        recyclerView.setAdapter(adapter);
    }
}
