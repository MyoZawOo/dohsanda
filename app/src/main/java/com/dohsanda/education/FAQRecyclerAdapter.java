package com.dohsanda.education;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.model.Answer;
import com.dohsanda.dohsanda.model.Question;

import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by myozawoo on 9/19/15.
 */
public class FAQRecyclerAdapter extends
    ExpandableRecyclerAdapter<FAQRecyclerAdapter.QuestionViewHolder, FAQRecyclerAdapter.AnswerViewHolder> {

    private int mSize = 0;
    private Context mContext;

    public FAQRecyclerAdapter(List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        mSize = parentItemList.size();
    }

    //    public FAQRecyclerAdapter() {
//        int total = QUESTIONS.length;
//        for (int i = 0; i < total; i++) {
//            expandState.append(i, false);
//        }
//    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public QuestionViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_faq_question, viewGroup, false);
        return new QuestionViewHolder(view);
    }

    @Override
    public AnswerViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_faq_answer, viewGroup, false);
        return new AnswerViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(QuestionViewHolder questionViewHolder, int i,
                                       ParentListItem parentListItem) {
        Question question = (Question) parentListItem;
        questionViewHolder.faqQuestion.setText(question.getText());
        mmtext.prepareView(mContext, questionViewHolder.faqQuestion, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(mContext, questionViewHolder.fagQuestionLabel, mmtext.TEXT_UNICODE, true, true);
    }

    @Override
    public void onBindChildViewHolder(AnswerViewHolder answerViewHolder, int i, Object childListItem) {
        Answer answer = (Answer) childListItem;
        answerViewHolder.faqAnswer.setText(answer.getText());
        mmtext.prepareView(mContext, answerViewHolder.faqAnswer, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(mContext, answerViewHolder.fagAnswerLabel, mmtext.TEXT_UNICODE, true, true);
    }

//    @Override
//    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        final Resources resource = context.getResources();
//
//        holder.faqQuestion.setText(QUESTIONS[position]);
//        holder.faqAnswer.setText(ANSWERS[position]);
//        holder.faqAnswer.setMovementMethod(ScrollingMovementMethod.getInstance());
//        holder.itemView.setBackgroundColor(resource.getColor(R.color.white));
////        holder.expandableLayout.setBackgroundColor(resource.getColor(R.color.white));
////        holder.expandableLayout.setInterpolator(Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR));
////        holder.expandableLayout.setExpanded(expandState.get(position));
//
//        mmtext.prepareView(context, holder.faqQuestion, mmtext.TEXT_UNICODE, true, true);
//        mmtext.prepareView(context, holder.fagQuestionLabel, mmtext.TEXT_UNICODE, true, true);
//        mmtext.prepareView(context, holder.faqAnswer, mmtext.TEXT_UNICODE, true, true);
//        mmtext.prepareView(context, holder.fagAnswerLabel, mmtext.TEXT_UNICODE, true, true);
//
////        holder.expandableLayout.setListener(new ExpandableLayoutListener() {
////            @Override
////            public void onAnimationStart() {
////            }
////
////            @Override
////            public void onAnimationEnd() {
////
////            }
////
////            @Override
////            public void onPreOpen() {
////                //createRotateAnimator(holder.buttonLayout, 0f, 90f).start();
////            }
////
////            @Override
////            public void onPreClose() {
////                //createRotateAnimator(holder.buttonLayout, 90f, 0f).start();
////
////            }
////
////            @Override
////            public void onOpened() {
////                expandState.put(position, true);
////            }
////
////            @Override
////            public void onClosed() {
////                expandState.put(position, false);
////            }
////        });
//    }
//
//    @Override
//    public void onClick(View view) {
//        ViewHolder holder = (ViewHolder) view.getTag();
//        if (view.getId() == R.id.faq_answer) {
//            mOnClickQAListener.onFaqAnswerClick(holder.expandableLayout);
//        } else if (view.getId() == R.id.faq_question) {
//            mOnClickQAListener.onFaqQuestionClick(holder.expandableLayout);
//        }
//    }

    @Override
    public int getItemCount() {
        return mSize;
    }

    public static class QuestionViewHolder extends ParentViewHolder {
        private static final float INITIAL_POSITION = 0.0f;
        private static final float ROTATED_POSITION = 180f;
        private static final float PIVOT_VALUE = 0.5f;
        private static final long DEFAULT_ROTATE_DURATION_MS = 200;
        private static final boolean HONEYCOMB_AND_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;

        public TextView faqQuestion;
        public TextView fagQuestionLabel;

        public QuestionViewHolder(View v) {
            super(v);
            faqQuestion = (TextView) v.findViewById(R.id.faq_question);
            fagQuestionLabel = (TextView) v.findViewById(R.id.question_label);
        }

        @SuppressLint("NewApi")
        @Override
        public void setExpanded(boolean expanded) {
            super.setExpanded(expanded);
            if (!HONEYCOMB_AND_ABOVE) {
                return;
            }

//            if (expanded) {
//                mArrowExpandImageView.setRotation(ROTATED_POSITION);
//            } else {
//                mArrowExpandImageView.setRotation(INITIAL_POSITION);
//            }
        }

        @Override
        public void onExpansionToggled(boolean expanded) {
            super.onExpansionToggled(expanded);
            if (!HONEYCOMB_AND_ABOVE) {
                return;
            }

            RotateAnimation rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, PIVOT_VALUE,
                    RotateAnimation.RELATIVE_TO_SELF, PIVOT_VALUE);
            rotateAnimation.setDuration(DEFAULT_ROTATE_DURATION_MS);
            rotateAnimation.setFillAfter(true);
            //mArrowExpandImageView.startAnimation(rotateAnimation);
        }
    }

    public static class AnswerViewHolder extends ChildViewHolder {
        public TextView faqAnswer;
        public TextView fagAnswerLabel;

        public AnswerViewHolder(View v) {
            super(v);
            faqAnswer = (TextView) v.findViewById(R.id.faq_answer);
            fagAnswerLabel = (TextView) v.findViewById(R.id.answer_label);
        }
    }

    public interface OnClickQAListener {
        void onFaqQuestionClick(View expandableRelativeLayout);
        void onFaqAnswerClick(View expandableRelativeLayout);
    }
}
