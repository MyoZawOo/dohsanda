package com.dohsanda.education;

/**
 * Created by myozawoo on 9/14/15.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mm.technomation.tmmtextutilities.mmtext;


public class ViewPagerFragment extends Fragment{

    final static String ARG_LAYOUT_ID = "com.dohsanda.education.ViewPagerFragment.ARG_LAYOUT_ID";

    public static ViewPagerFragment newInstance(int layoutId) {
        ViewPagerFragment pane = new ViewPagerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_ID, layoutId);
        pane.setArguments(args);
        return pane;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(getArguments().getInt(ARG_LAYOUT_ID, -1),
                container, false);
        mmtext.prepareViewGroup(container.getContext(), rootView, mmtext.TEXT_UNICODE, true ,true);
        return rootView;
    }
}
