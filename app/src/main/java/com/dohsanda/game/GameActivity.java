package com.dohsanda.game;


/**
 * Created by myozawoo on 8/19/15.
 */


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dohsanda.dohsanda.MainActivity;
import com.dohsanda.dohsanda.R;

import java.util.ArrayList;
import java.util.Random;

import mm.technomation.tmmtextutilities.mmtext;

public class GameActivity extends Activity {

    Button btnReady;

    Cursor c;
    int screenwidth = 0;
    boolean correctAnswer = true;
    ImageView imgQuestion;
    Button btnTrue;
    Button btnFalse;
    LifeTime lifeTime;

    SoundPool mSoundPool;
    int soundCorrect;
    int sountWrong;

    boolean isClosed = false;
    private static final String SHOWCASE_ID = "simple example";

    int gameStart = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);

        mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundCorrect = mSoundPool.load(getApplicationContext(), R.raw.correctanswer, 1);
        sountWrong = mSoundPool.load(getApplicationContext(), R.raw.wronganswer, 1);

        final DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        screenwidth = displaymetrics.widthPixels;

        imgQuestion = (ImageView) findViewById(R.id.imgQuestion);

        btnTrue = (Button) findViewById(R.id.btnTrue);
        btnFalse = (Button) findViewById(R.id.btnFalse);
        btnReady = (Button) findViewById(R.id.btnReady);

        Button btnRetry = (Button) findViewById(R.id.btnRetry);
        Button btnMenu = (Button) findViewById(R.id.btnMenu);

        lifeTime = new LifeTime();
        lifeTime.gameOver = true;

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);

        btnMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                finish();
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                RelativeLayout lyResult = (RelativeLayout) findViewById(R.id.lyResult);
                lyResult.clearAnimation();
                lyResult.setVisibility(RelativeLayout.INVISIBLE);
                LinearLayout lyIntro = (LinearLayout) findViewById(R.id.lyIntro);
                lyIntro.clearAnimation();
                lyIntro.setVisibility(LinearLayout.VISIBLE);
                new IntroDisappear().execute();
            }
        });


        btnReady.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
            isClosed = false;
            gameStart ++;
            new IntroDisappear().execute();
            }
        });

        btnTrue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                btnTrue.setVisibility(ImageButton.INVISIBLE);
                btnFalse.setVisibility(ImageButton.INVISIBLE);

                if (correctAnswer == true)// the Answer is correct=
                    //new CheckAnswer(true).execute();
                    CheckAnswer(true);
                else
                    //new CheckAnswer(false).execute();
                    CheckAnswer(false);
            }
        });

        btnFalse.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                btnTrue.setVisibility(ImageButton.INVISIBLE);
                btnFalse.setVisibility(ImageButton.INVISIBLE);

                if (correctAnswer == false)// the Answer is correct
                    //new CheckAnswer(true).execute();
                    CheckAnswer(true);
                else
                    //new CheckAnswer(false).execute();
                    CheckAnswer(false);
            }
        });
    }

    private void CheckAnswer(final boolean correct) {
        new Thread() {
            @Override
            public void run() {
                if (!correct)
                    Handler_CheckAnswerPre.sendEmptyMessage(0);
                else
                    Handler_CheckAnswerPre.sendEmptyMessage(1);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Handler_CheckAnswerPost.sendEmptyMessage(0);
            }
        }.start();
    }

    Handler Handler_CheckAnswerPre = new Handler() {
        public void handleMessage(android.os.Message msg) {
            final ImageView imgAnswer = (ImageView) findViewById(R.id.imgAnswer);
            if (msg.what == 1) //Correct
            {
                mSoundPool.play(soundCorrect, 1f, 1f, 0, 0, 1);
                if (correctAnswer == true)
                    imgAnswer.setImageResource(R.drawable.correct);
                else
                    imgAnswer.setImageResource(R.drawable.correct);
                Animation slide_leftright = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_leftright);
                imgAnswer.clearAnimation();
                imgAnswer.startAnimation(slide_leftright);

                lifeTime.progress += 15;
                lifeTime.speed -= 5;
                lifeTime.score++;
            } else if (msg.what == 0) {
                mSoundPool.play(sountWrong, 1f, 1f, 0, 0, 1);
                if (correctAnswer == true)

                    imgAnswer.setImageResource(R.drawable.wrong);

                else

                    imgAnswer.setImageResource(R.drawable.wrong);


                Animation blink = AnimationUtils.loadAnimation(getBaseContext(), R.anim.blink);
                imgAnswer.clearAnimation();
                imgAnswer.startAnimation(blink);

                lifeTime.progress -= (5 + lifeTime.wrongIncrease);
                lifeTime.wrongIncrease++;
            }
        }

        ;
    };

    private Handler Handler_CheckAnswerPost = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (!lifeTime.gameOver) {
                btnTrue.setVisibility(ImageButton.VISIBLE);
                btnFalse.setVisibility(ImageButton.VISIBLE);
                setQuestion();
            }
        }

        ;
    };

    ArrayList<Integer> randomQuestion;


    private void randomNumList() {
        randomQuestion = new ArrayList<Integer>();
        Random r = new Random();

        for (int i = 0; i < 16; i++) {

            int rNum = r.nextInt(16);
            for (int j = 0; j < randomQuestion.size(); j++) {
                if (randomQuestion.get(j) != rNum) {
                    if (j == randomQuestion.size() - 1) //is this last array
                    {
                        break;
                    }
                } else {
                    rNum = r.nextInt(16);
                    j = -1;
                }
            }
            randomQuestion.add(rNum);

        }
    }

    class IntroDisappear extends AsyncTask<Void, Integer, Void> {
        TextView txtCount;
        int count = 3;

        @Override
        protected void onPreExecute() {
            txtCount = (TextView) findViewById(R.id.txtCount);
            ImageView imgIntro = (ImageView) findViewById(R.id.imgIntro);
            TextView txtInfo = (TextView) findViewById(R.id.txtInfo);
            imgIntro.setVisibility(ImageView.GONE);
            txtInfo.setVisibility(View.INVISIBLE);
            btnReady.setVisibility(TextView.GONE);
            txtCount.setVisibility(TextView.VISIBLE);
            randomNumList();
            t = -1;

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            txtCount.setText(values[0] + "");
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
            if (!isClosed) {
                LinearLayout lyIntro = (LinearLayout) findViewById(R.id.lyIntro);
                lyIntro.setVisibility(RelativeLayout.GONE);
                Animation moveouttoup = AnimationUtils.loadAnimation(getBaseContext(), R.anim.moveouttoup);
                lyIntro.clearAnimation();
                lyIntro.startAnimation(moveouttoup);

                lifeTime = new LifeTime();
                lifeTime.execute();
                super.onPostExecute(result);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            for (int i = 0; i <= 3; i++) {
                publishProgress(count - i);
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            return null;
        }

    }

    class LifeTime extends AsyncTask<Void, Void, Void> {
        View progressBar;
        TextView txtProgress;
        int progress = 999999;
        boolean gameOver = false;
        int speed = 200;
        int wrongIncrease = 0;
        int score = 0;

        @Override
        protected void onPreExecute() {
            setQuestion();
            btnTrue.setVisibility(ImageButton.VISIBLE);
            btnFalse.setVisibility(ImageButton.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            if (progress < 0)
                progress = 0;
//            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(screenwidth * progress/100, LayoutParams.MATCH_PARENT, Gravity.BOTTOM);
            //          progressBar.setLayoutParams(lp);
//            txtProgress.setText(progress + "");
            super.onProgressUpdate(values);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            while (!gameOver) {
                try {
                    progress--;
                    if (progress <= 0)
                        gameOver = true;
                    publishProgress();
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            btnTrue.setVisibility(ImageButton.INVISIBLE);
            btnFalse.setVisibility(ImageButton.INVISIBLE);
            GameOver(score);
            super.onPostExecute(result);
        }
    }

    class ButtonTempDisable extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Button btnRetry = (Button) findViewById(R.id.btnRetry);
            Button btnMenu = (Button) findViewById(R.id.btnMenu);

            btnRetry.setClickable(true);
            btnMenu.setClickable(true);
            super.onPostExecute(result);
        }
    }

    private void GameOver(int score) {
        TextView txtScore = (TextView) findViewById(R.id.txtScore);

        String[] marks = {"၀", "၁", "၂", "၃", "၄", "၅", "၆", "၇",
                "၈", "၉", "၁၀", "၁၁", "၁၂", "၁၃", "၁၄", "၁၅", "၁၆" };

        txtScore.setText("၁၆ ခုတွင် " + marks[score] + " ခုမှန်ပါသည်။");
        mmtext.prepareView(getBaseContext(), txtScore, mmtext.TEXT_UNICODE, true, true);

        RelativeLayout lyResult = (RelativeLayout) findViewById(R.id.lyResult);
        lyResult.setVisibility(RelativeLayout.VISIBLE);
        Animation moveintodown = AnimationUtils.loadAnimation(getBaseContext(), R.anim.moveintodown);
        lyResult.clearAnimation();
        lyResult.startAnimation(moveintodown);

        Button btnRetry = (Button) findViewById(R.id.btnRetry);
        Button btnMenu = (Button) findViewById(R.id.btnMenu);

        btnRetry.setClickable(false);
        btnMenu.setClickable(false);

        new ButtonTempDisable().execute();
    }

    int t = -1;

    private void setQuestion() {
        t++;

        if (t == 16) {
            lifeTime.gameOver = true;
        } else {

            String[] ans = {"true", "false", "true", "false", "true", "false", "true", "false",
                    "true", "false", "true", "false", "true", "false", "true", "false"};
            int[] question = {R.drawable.valid_one, R.drawable.invalid_one, R.drawable.valid_two, R.drawable.invalid_two,
                    R.drawable.valid_three, R.drawable.invalid_three, R.drawable.valid_four, R.drawable.invalid_four,
                    R.drawable.valid_five, R.drawable.invalid_five, R.drawable.valid_six, R.drawable.invalid_six,
                    R.drawable.valid_seven, R.drawable.invalid_seven, R.drawable.valid_eight, R.drawable.invalid_eight};

            ImageView imgAnswer = (ImageView) findViewById(R.id.imgAnswer);
            imgAnswer.setImageResource(R.drawable.blah);

            imgQuestion.setImageResource(question[randomQuestion.get(t)]);
            correctAnswer = Boolean.parseBoolean(ans[randomQuestion.get(t)]);

            Animation slide_leftright = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_leftright);
            imgAnswer.clearAnimation();
            imgQuestion.clearAnimation();
            imgAnswer.startAnimation(slide_leftright);
            imgQuestion.startAnimation(slide_leftright);
        }
    }

    @Override
    public void onBackPressed() {
        if (! lifeTime.gameOver) {
            String play = mmtext.processText("ဆက်မဆော့တော့ဘူးလား", mmtext.TEXT_UNICODE, true, true);
            showAlertDialog(play);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        isClosed = true;
        super.onDestroy();
    }

    private void showAlertDialog(String msg) {
        TextView tv =new  TextView(this);
        tv.setText(msg);
        tv.setPadding(32, 32, 32, 16);

        mmtext.prepareView(this, tv, mmtext.TEXT_UNICODE, true, true);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(tv)
                .setPositiveButton(getString(R.string.label_game_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    }
                }).setNegativeButton(R.string.label_game_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    lifeTime.gameOver = true;
                    }
                })
                .show();

        Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        Button cancelButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        mmtext.prepareView(this, okButton, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, cancelButton, mmtext.TEXT_UNICODE, true, true);
        cancelButton.setTextColor(getResources().getColor(R.color.material_red_800));
    }
}


