package com.dohsanda.dohsanda.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by arkar on 9/20/15.
 */
public class EmbeddedTextView extends TextView {

    public EmbeddedTextView(Context context) {
        super(context);
        setFontEmbed(context);
    }

    public EmbeddedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFontEmbed(context);
    }

    public EmbeddedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFontEmbed(context);
    }

    private void setFontEmbed(Context context) {
        mmtext.prepareView(context, this, mmtext.TEXT_UNICODE, true, true);
    }
}