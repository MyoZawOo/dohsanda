package com.dohsanda.dohsanda.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.utils.TextUtils;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.StateAPIHelper;
import org.maepaysoh.maepaysohsdk.models.Candidate;
import org.maepaysoh.maepaysohsdk.models.Constituency;
import org.maepaysoh.maepaysohsdk.models.Party;

import mm.technomation.tmmtextutilities.mmtext;

import static com.dohsanda.dohsanda.utils.TextUtils.checkDataIsProvided;

public class CandidateComparisonActivity extends BaseActivity {

    public static final String EXTRA_CADIDATE_ONE =
            "org.maepaesoh.maepaesoh.ui.CandidateListActivity.EXTRA_CADIDATE_ONE";
    public static final String EXTRA_CADIDATE_TWO =
            "org.maepaesoh.maepaesoh.ui.CandidateListActivity.EXTRA_CADIDATE_TWO";

    private Toolbar mToolbar;
    private View mToolbarShadow;
    private ImageView mFirstCandidateImage;
    private ImageView mSecondCandidateImage;
    private TextView mFirstCandidateName;
    private TextView mSecondCandidateName;
    private TextView mFirstPartyName;
    private TextView mSecondPartyName;
    private TextView mFirstCandidateEducation;
    private TextView mSecondCandidateEducation;
    private TextView mFirstCandidateOccupation;
    private TextView mSecondCandidateOccupation;
    private TextView mFirstCandidateAddress;
    private TextView mSecondCandidateAddress;
    private TextView mFirstCandidateER;
    private TextView mSecondCandidateER;
    private TextView mFirstCandidatePrev;
    private TextView mSecondCandidatePrev;
    private TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_comparison);

        mToolbar = (Toolbar) findViewById(R.id.candidate_comparison_toolbar);
        mToolbarShadow = findViewById(R.id.candidate_comparison_toolbar_shadow);
        mToolbar.setBackgroundColor(getResources().getColor(R.color.candidate));
        mToolbar.setTitle(getString(R.string.title_activity_candidate_comparison));
        hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);

        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            // Showing Back Arrow  <-
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        Candidate candidateOne = (Candidate) getIntent().getSerializableExtra(EXTRA_CADIDATE_ONE);
        Candidate candidateTwo = (Candidate) getIntent().getSerializableExtra(EXTRA_CADIDATE_TWO);

        mTitle = (TextView) findViewById(R.id.title);
        mTitle.setText(formatComparisonTitle(candidateOne));

        mFirstCandidateImage = (ImageView) findViewById(R.id.first_candidate_image);
        mFirstCandidateName = (TextView) findViewById(R.id.first_candidate_name);
        mFirstPartyName = (TextView) findViewById(R.id.first_candidate_party);
        mFirstCandidateEducation = (TextView) findViewById(R.id.first_candidate_education);
        mFirstCandidateOccupation = (TextView) findViewById(R.id.first_candidate_occupation);
        mFirstCandidateAddress = (TextView) findViewById(R.id.first_candidate_address);
        mFirstCandidateER = (TextView) findViewById(R.id.first_candidate_er);
        mFirstCandidatePrev = (TextView) findViewById(R.id.first_candidate_prev);
        mSecondCandidateImage = (ImageView) findViewById(R.id.second_candidate_image);
        mSecondCandidateName = (TextView) findViewById(R.id.second_candidate_name);
        mSecondPartyName = (TextView) findViewById(R.id.second_candidate_party);
        mSecondCandidateEducation = (TextView) findViewById(R.id.second_candidate_education);
        mSecondCandidateOccupation = (TextView) findViewById(R.id.second_candidate_occupation);
        mSecondCandidateAddress = (TextView) findViewById(R.id.second_candidate_address);
        mSecondCandidateER = (TextView) findViewById(R.id.second_candidate_er);
        mSecondCandidatePrev = (TextView) findViewById(R.id.second_candidate_prev);

        setupFirstCandidate(candidateOne);
        setupSecondCandidate(candidateTwo);

//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.candidate_one_container, CandidateDetailFragment
//                        .newInstance(candidateOne, true))
//                .commit();
//        fragmentManager.beginTransaction()
//                .replace(R.id.candidate_two_container, CandidateDetailFragment
//                        .newInstance(candidateTwo, true))
//                .commit();

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
    }

    private String formatComparisonTitle(Candidate candidate) {
        Constituency constituency = candidate.getConstituency();
        String titleTemplate = "%s မှ ဝင်ရောက်ယှဉ်ပြိုင်မည့် %sကိုယ်စားလှယ်နှစ်ဦး နှိုင်းယှဉ်ချက်";
        String constituencyText;
        String candidateLabel = getString(R.string.CandidateConstituencyNo);
        MaePaySohApiWrapper maePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
        StateAPIHelper stateAPIHelper = maePaySohApiWrapper.getStateApiHelper();
        if (constituency.getaMPCode() != null) {
            String ampCodeEng = constituency.getaMPCode();
            ampCodeEng = ampCodeEng.substring(ampCodeEng.length() - 2, ampCodeEng.length());
            String ampCodeMM = TextUtils.convertToMM(String.valueOf(Integer.parseInt(ampCodeEng)));
            constituencyText = stateAPIHelper.getStateById(constituency.getsTPCode()).getName()
                    + " " + candidateLabel + " (" + ampCodeMM + ")";
        } else {
            constituencyText = constituency.getName();
        }

        return String.format(titleTemplate, constituencyText, candidate.getLegislature());
    }

    private void setupFirstCandidate(Candidate candidate) {
        Glide.with(this)
                .load(candidate.getPhotoUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mFirstCandidateImage);
        mFirstCandidateName.setText(candidate.getName());
        String partyName = (candidate.getParty() == null) ? candidate.getPartyName() :
                candidate.getParty().getPartyName();
        if (partyName == null || partyName.isEmpty()) {
            partyName = getString(R.string.label_single_candidate);
        } else {
            mFirstPartyName.setOnClickListener(getOnPartyClickListener(candidate.getParty()));
        }
        mFirstPartyName.setText(partyName);
        mFirstCandidateEducation.setText(checkDataIsProvided(candidate.getEducation(), this));
        mFirstCandidateOccupation.setText(checkDataIsProvided(candidate.getOccupation(), this));
        mFirstCandidateAddress.setText(checkDataIsProvided(candidate.getWardVillage(), this));
        mFirstCandidateER.setText(checkDataIsProvided(candidate.getEthnicity(), this) + "/"
            + checkDataIsProvided(candidate.getReligion(), this));
        if (candidate.getMpid() == null) {
            mFirstCandidatePrev.setText(getString(R.string.CandidatePreviousNo));
        } else {
            mFirstCandidatePrev.setText(getString(R.string.CandidatePreviousYes));
        }
    }

    private void setupSecondCandidate(final Candidate candidate) {
        Glide.with(this)
                .load(candidate.getPhotoUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mSecondCandidateImage);
        mSecondCandidateName.setText(candidate.getName());
        String partyName = (candidate.getParty() == null) ? candidate.getPartyName() :
                candidate.getParty().getPartyName();
        if (partyName == null || partyName.isEmpty()) {
            partyName = getString(R.string.label_single_candidate);
        } else {
            mSecondPartyName.setOnClickListener(getOnPartyClickListener(candidate.getParty()));
        }
        mSecondPartyName.setText(partyName);
        mSecondCandidateEducation.setText(checkDataIsProvided(candidate.getEducation(), this));
        mSecondCandidateOccupation.setText(checkDataIsProvided(candidate.getOccupation(), this));
        mSecondCandidateAddress.setText(checkDataIsProvided(candidate.getWardVillage(), this));
        mSecondCandidateER.setText(checkDataIsProvided(candidate.getEthnicity(), this) + "/"
                + checkDataIsProvided(candidate.getReligion(), this));
        if (candidate.getMpid() == null) {
            mSecondCandidatePrev.setText(getString(R.string.CandidatePreviousNo));
        } else {
            mSecondCandidatePrev.setText(getString(R.string.CandidatePreviousYes));
        }
    }


    private View.OnClickListener getOnPartyClickListener(final Party party) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent partyIntent = new Intent(CandidateComparisonActivity.this, PartyDetailActivity.class);
                partyIntent.putExtra(PartyDetailActivity.PARTY_CONSTANT, party);
                startActivity(partyIntent);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_candidate_comparison, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
