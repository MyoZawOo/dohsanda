package com.dohsanda.dohsanda.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.utils.TimeUtils;

import org.maepaysoh.maepaysohsdk.models.Party;

import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;

import static com.dohsanda.dohsanda.utils.TextUtils.checkDataIsProvided;
import static com.dohsanda.dohsanda.utils.TextUtils.convertToMM;

/**
 * Created by yemyatthu on 8/4/15.
 */
public class PartyDetailActivity extends BaseActivity {
  public static final String PARTY_CONSTANT =
      "org.maepaesoh.maepaesoh.ui.PartyDetailActivity.PARTY_CONSTANT";

  // Ui elements
  private Toolbar mToolbar;
  private View mToolbarShadow;

  private ImageView mPartyFlag;
  private TextView mPartyNameMyanmar;
  private TextView mPartyNameEnglish;
  private TextView mPartyLeader;
  private TextView mPartyChairman;
  private TextView mPartyMemberCount;
  private TextView mPartyEstbDate;
  private TextView mPartyEstbApprovalDate;
  private TextView mPartyRegApplicationDate;
  private TextView mPartyRegApprovalDate;
  private TextView mPartyApprovedNo;
  private TextView mPartyRegion;
  private TextView mPartyHeadquarters;
  private TextView mPartyContact;
  private Button mPartyPolicy;
  private Party mParty;
  private ImageView mPartySeal;

  private ShareActionProvider mShareActionProvider;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_party_details);

    mToolbar = (Toolbar) findViewById(R.id.party_detail_toolbar);
    mToolbarShadow = findViewById(R.id.party_detail_toolbar_shadow);

    hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);

    mPartyFlag = (ImageView) findViewById(R.id.party_flag);
    mPartyNameMyanmar = (TextView) findViewById(R.id.party_name_myanmar);
    //mPartyNameEnglish = (TextView) findViewById(R.id.party_name_english);
    mPartyLeader = (TextView) findViewById(R.id.party_leader);
    mPartyChairman = (TextView) findViewById(R.id.party_chairman);
    mPartyMemberCount = (TextView) findViewById(R.id.party_member_count);
    mPartyEstbDate = (TextView) findViewById(R.id.party_estb_date);
    mPartyEstbApprovalDate = (TextView) findViewById(R.id.party_estb_approval_date);
    mPartyRegApplicationDate = (TextView) findViewById(R.id.party_reg_application_date);
    mPartyRegApprovalDate = (TextView) findViewById(R.id.party_reg_approval_date);
    mPartyApprovedNo = (TextView) findViewById(R.id.party_approved_no);
    mPartyRegion = (TextView) findViewById(R.id.party_region);
    mPartyHeadquarters = (TextView) findViewById(R.id.party_headquarters);
    mPartyContact = (TextView) findViewById(R.id.party_contact);
    mPartyPolicy = (Button) findViewById(R.id.policy_btn);
    mPartySeal = (ImageView) findViewById(R.id.party_seal);

    setSupportActionBar(mToolbar);
    ActionBar mActionBar = getSupportActionBar();
    if (mActionBar != null) {
      // Showing Back Arrow  <-
      mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    mParty = (Party) getIntent().getSerializableExtra(PARTY_CONSTANT);
    if (mParty != null) {
      Glide.with(this).load(mParty.getPartyFlag()).into(mPartyFlag);
      Glide.with(this).load(mParty.getPartySeal()).into(mPartySeal);
      mActionBar.setTitle(mParty.getPartyName());
      //mPartyNameEnglish.setText(mParty.getPartyNameEnglish());
      mPartyNameMyanmar.setText(mParty.getPartyName());
      List<String> leaders = mParty.getLeadership();
      for (String leader : leaders) {
        if (leaders.indexOf(leader) == leaders.size() - 1) {
          mPartyLeader.append(leader);
        } else {
          mPartyLeader.append(leader + "၊ ");
        }
      }
      List<String> chairmen = mParty.getChairman();
      for (String chairman : chairmen) {
        if (leaders.indexOf(chairman) == chairmen.size() - 1) {
          mPartyChairman.append(chairman);
        } else {
          mPartyChairman.append(chairman + "၊ ");
        }
      }
      mPartyMemberCount.setText(checkDataIsProvided(mParty.getMemberCount(), this));
      mPartyEstbDate.setText(convertToMM(checkDataIsProvided(TimeUtils.convertISO8601toDateFormat(
              mParty.getEstablishmentDate()), this)));
      mPartyEstbApprovalDate.setText(convertToMM(checkDataIsProvided(
              TimeUtils.convertISO8601toDateFormat(mParty.getEstablishmentApprovalDate()), this)));
      mPartyRegApplicationDate.setText(convertToMM(checkDataIsProvided(
              TimeUtils.convertISO8601toDateFormat(mParty.getRegistrationApplicationDate()), this)));
      mPartyRegApprovalDate.setText(convertToMM(checkDataIsProvided(
              TimeUtils.convertISO8601toDateFormat(mParty.getRegistrationApprovalDate()), this)));
      mPartyApprovedNo.setText(checkDataIsProvided(mParty.getApprovedPartyNumber(), this));
      mPartyRegion.setText(mParty.getRegion());
      mPartyHeadquarters.setText(mParty.getHeadquarters());
      List<String> contacts = mParty.getContact();
      for (String contact : contacts) {
        if (contacts.indexOf(contact) == contacts.size() - 1) {
          mPartyContact.append(contact);
        } else {
          mPartyContact.append(contact + "၊ ");
        }
      }
      mPartyPolicy.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Intent i = new Intent(Intent.ACTION_VIEW);
          i.setData(Uri.parse(mParty.getPolicy()));
          startActivity(i);
        }
      });
      Linkify.addLinks(mPartyPolicy, Linkify.WEB_URLS);
    }

    Button mCandidateByPartyBtn = (Button) findViewById(R.id.candidate_by_party_btn);
    mCandidateByPartyBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(PartyDetailActivity.this, CandidateListActivity.class);
        intent.putExtra(CandidateListActivity.EXTRA_PARTY_ID, mParty.getPartyId());
        intent.putExtra(CandidateListActivity.EXTRA_PARTY_NAME, mParty.getPartyName());
        startActivity(intent);
      }
    });

    mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
  }

//  @Override public boolean onCreateOptionsMenu(Menu menu) {
//    getMenuInflater().inflate(R.menu.menu_party_detail, menu);
//    return true;
//  }
//

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

}
