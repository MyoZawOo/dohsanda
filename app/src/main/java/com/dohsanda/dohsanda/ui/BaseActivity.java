package com.dohsanda.dohsanda.ui;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.utils.ZgDetector;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.rabbitconverter.rabbit.Rabbit;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by Ye Lin Aung on 15/08/03.
 */
public class BaseActivity extends AppCompatActivity {
  private static MaePaySohApiWrapper mMaePaySohApiWrapper;

  @Override public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
    super.onCreate(savedInstanceState, persistentState);
  }

  protected void hideToolBarShadowForLollipop(Toolbar mToolbar, View shadowView) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
      // only for lollipop and newer versions
      shadowView.setVisibility(View.GONE);
      mToolbar.setElevation(getResources().getDimension(R.dimen.toolbar_elevation_height));
    }
  }

  protected void showErrorAlertDialog(String message) {
    TextView tv = new TextView(this);
    tv.setText(message);
    tv.setPadding(32, 40, 32, 16);
    mmtext.prepareView(this, tv, mmtext.TEXT_UNICODE, true, true);
    AlertDialog dialog = new AlertDialog.Builder(this)
            .setView(tv)
            .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
              }
            }).show();
    Button positiveBtn = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
    mmtext.prepareView(this, positiveBtn, mmtext.TEXT_UNICODE, true, true);
  }

  protected String checkAndConvertZg(String str) {
    str = str.trim();
    if (! ZgDetector.isUni(str)) {
      str = str.replaceAll("\\p{C}", "");
      str = Rabbit.zg2uni(str);
    }
    str = str.replace("ဦး", "ဦး");
    return str;
  }

  protected String formatRequiredErrorMessage(String name) {
    return mmtext.processText(name + " " +
            getString(R.string.message_require), mmtext.TEXT_UNICODE, true, true);
  }

  protected void share(String title, String body) {
    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
    sharingIntent.setType("text/plain");
    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title);
    sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
    startActivity(Intent.createChooser(sharingIntent, "Share via"));
  }

  @TargetApi(21)
  protected void setStatusBarColor(int color) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      window.setStatusBarColor(getResources().getColor(color));
    }
  }
}
