package com.dohsanda.dohsanda.ui;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.dohsanda.dohsanda.Constants;
import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.adapters.CandidateAdapter;
import com.dohsanda.dohsanda.adapters.EndlessRecyclerViewAdapter;
import com.dohsanda.dohsanda.utils.InternetUtils;
import com.dohsanda.dohsanda.utils.Logger;
import com.dohsanda.dohsanda.utils.ViewUtils;

import org.maepaysoh.maepaysohsdk.CandidateAPIHelper;
import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.StateAPIHelper;
import org.maepaysoh.maepaysohsdk.models.Candidate;
import org.maepaysoh.maepaysohsdk.models.CandidateListReturnObject;
import org.maepaysoh.maepaysohsdk.models.State;
import org.maepaysoh.maepaysohsdk.utils.CandidateAPIProperties;
import org.maepaysoh.maepaysohsdk.utils.CandidateAPIPropertiesMap;

import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;
import retrofit.RetrofitError;

/**
 * Created by Ye Lin Aung on 15/08/04.
 */
public class CandidateListActivity extends BaseActivity
    implements CandidateAdapter.ClickInterface {

  private static String TAG = Logger.makeLogTag(CandidateListActivity.class);
  public static final String EXTRA_PARTY_ID =
          "org.maepaesoh.maepaesoh.ui.CandidateListActivity.EXTRA_PARTY_ID";
  public static final String EXTRA_PARTY_NAME =
          "org.maepaesoh.maepaesoh.ui.CandidateListActivity.EXTRA_PARTY_NAME";

  // Ui components
  private Toolbar mToolbar;
  private View mToolbarShadow;
  private RecyclerView mCandidateListRecyclerView;
  private ProgressBar mProgressView;
  private View mErrorView;
  private Button mRetryBtn;
  private CandidateAPIHelper mCandidateAPIHelper;
  private TextView mErrorText;
  private TextView mTitleView;
  private Spinner mStateSpinner;

  private TokenKeyGenerateClass mTokenClass;
  private ViewUtils viewUtils;
  private List<Candidate> mCandidates = new ArrayList<>();
  private List<Candidate> mSearchCandidates = new ArrayList<>();
  private LinearLayoutManager mLayoutManager;
  private CandidateAdapter mCandidateAdapter;
  private EndlessRecyclerViewAdapter mEndlessRecyclerViewAdapter;
  private int mCurrentPage = 1;
  private MaePaySohApiWrapper mMaePaySohApiWrapper;
  private DownloadCandidateListAsync mDownloadCandidateListAsync;
  private SearchCandidateTask mSearchCandidateTask;
  private CandidateAPIPropertiesMap mCandidateAPIPropertiesMap;
  private String mSearchName = "";

  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_candidate_list);

    mToolbar = (Toolbar) findViewById(R.id.candidate_list_toolbar);
    mToolbarShadow = findViewById(R.id.candidate_list_toolbar_shadow);
    mCandidateListRecyclerView = (RecyclerView) findViewById(R.id.candidate_list_recycler_view);
    mProgressView = (ProgressBar) findViewById(R.id.candidate_list_progress_bar);
    mErrorView = findViewById(R.id.candidate_list_error_view);
    mErrorText = (TextView) mErrorView.findViewById(R.id.error_view_error_text);
    mRetryBtn = (Button) mErrorView.findViewById(R.id.error_view_retry_btn);
    mTitleView = (TextView) findViewById(R.id.title);
    mStateSpinner = (Spinner) findViewById(R.id.state_spinner);
    mMaePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
    mCandidateAPIPropertiesMap = new CandidateAPIPropertiesMap();
    mCandidateAPIPropertiesMap.put(CandidateAPIProperties.CACHE, false);
    mProgressView.getIndeterminateDrawable()
        .setColorFilter(getResources().getColor(R.color.primary), PorterDuff.Mode.SRC_ATOP);

    hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);
    mToolbar.setBackgroundColor(getResources().getColor(R.color.candidate));
    setStatusBarColor(R.color.dark_candidate);

    setSupportActionBar(mToolbar);

    ActionBar mActionBar = getSupportActionBar();
    if (mActionBar != null) {
      // Showing Back Arrow  <-
      mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    viewUtils = new ViewUtils(this);

    String toolbarTitle = getString(R.string.CandidateList);
    if (getIntent().hasExtra(EXTRA_PARTY_ID)) {
      mCandidateAPIPropertiesMap.put(CandidateAPIProperties.PARTY, Integer.valueOf(getIntent()
              .getStringExtra(EXTRA_PARTY_ID)));
      toolbarTitle = getIntent().getStringExtra(EXTRA_PARTY_NAME);
    }

    mActionBar.setTitle(toolbarTitle);

    String title;
    if (getIntent().hasExtra(SearchActivity.EXTRA_SEARCH_TITLE)) {
      title = getIntent().getStringExtra(SearchActivity.EXTRA_SEARCH_TITLE);
      mTitleView.setText(title);
      setupSearchParam();
    } else {
      mTitleView.setVisibility(View.GONE);
      mStateSpinner.setVisibility(View.VISIBLE);
      setupStateSpinner();
    }

    mLayoutManager = new LinearLayoutManager(this);
    mCandidateListRecyclerView.setLayoutManager(mLayoutManager);
    mCandidateAdapter = new CandidateAdapter();
    mCandidateAdapter.setOnItemClickListener(this);
    mEndlessRecyclerViewAdapter = new EndlessRecyclerViewAdapter(this, mCandidateAdapter,
        new EndlessRecyclerViewAdapter.RequestToLoadMoreListener() {
          @Override public void onLoadMoreRequested() {
            downloadCandidateList();
          }
        });
    mCandidateListRecyclerView.setAdapter(mEndlessRecyclerViewAdapter);

    if (InternetUtils.isNetworkAvailable(this)) {
      viewUtils.showProgress(mCandidateListRecyclerView, mProgressView, true);
      checkAndCreateToken();
    } else {
      viewUtils.showProgress(mCandidateListRecyclerView, mErrorView, true);
      mProgressView.setVisibility(View.GONE);
    }

    mRetryBtn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (InternetUtils.isNetworkAvailable(CandidateListActivity.this)) {
          mErrorView.setVisibility(View.GONE);
          mProgressView.setVisibility(View.VISIBLE);
          checkAndCreateToken();
        }
      }
    });

    mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case android.R.id.home:
        onBackPressed();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void setupStateSpinner() {
    StateAPIHelper stateAPIHelper = mMaePaySohApiWrapper.getStateApiHelper();
    final List<State> states = stateAPIHelper.getAllStatesData();
    final List<String> stateList = new ArrayList<>();
    String prefix = mmtext.processText(getResources().getString(R.string.label_state_prefix),
            mmtext.TEXT_UNICODE, true, true);
    stateList.add(prefix);
    for (State state : states) {
      stateList.add(mmtext.processText(state.getName(), mmtext.TEXT_UNICODE, true, true));
    }
    ArrayAdapter mStateAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, R.id.item_spinner,
            stateList);
    mStateSpinner.setAdapter(mStateAdapter);
    mStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i > 0) {
          State state = states.get(--i);
          mCandidateAPIPropertiesMap.put(CandidateAPIProperties.ST_PCODE, state.getId());
          mEndlessRecyclerViewAdapter.onDataReady(false);
          mErrorView.setVisibility(View.GONE);
          viewUtils.showProgress(mCandidateListRecyclerView, mProgressView, true);
          mCandidates.clear();
          downloadCandidateList();
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });
  }

  private void setupSearchParam() {
    Intent intent = getIntent();
    if (intent.hasExtra(SearchActivity.EXTRA_SEARCH_STATE)) {
      mCandidateAPIPropertiesMap.put(CandidateAPIProperties.ST_PCODE, intent.getStringExtra(
              SearchActivity.EXTRA_SEARCH_STATE));
    }
    if (intent.hasExtra(SearchActivity.EXTRA_SEARCH_TOWNSHIP)) {
      mCandidateAPIPropertiesMap.put(CandidateAPIProperties.TS_PCODE, intent.getStringExtra(
              SearchActivity.EXTRA_SEARCH_TOWNSHIP));
    }
    if (intent.hasExtra(SearchActivity.EXTRA_SEARCH_LEGISLATURE)) {
      mCandidateAPIPropertiesMap.put(CandidateAPIProperties.LEGISLATURE, intent.getStringExtra(
              SearchActivity.EXTRA_SEARCH_LEGISLATURE));
    }

    if (intent.hasExtra(SearchActivity.EXTRA_SEARCH_NAME)) {
      mSearchName = intent.getStringExtra(SearchActivity.EXTRA_SEARCH_NAME);
    }
  }

  private void checkAndCreateToken() {
    String apiKey = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
            .getString(Constants.API_KEY, "");
    if (apiKey.length() > 0) {
      mMaePaySohApiWrapper.setTokenKey(apiKey);
      if (mSearchName.isEmpty()) {
        downloadCandidateList();
      } else {
        searchCandidateByName(mSearchName);
      }
    } else {
      mTokenClass = new TokenKeyGenerateClass();
      mTokenClass.execute();
    }
  }

  private CandidateAPIHelper getCandidateAPIHelper() {
    if (mCandidateAPIHelper == null) {
      mCandidateAPIHelper = mMaePaySohApiWrapper.getCandidateApiHelper();
    }
    return mCandidateAPIHelper;
  }

  private void downloadCandidateList() {
    mCandidateAPIPropertiesMap.put(CandidateAPIProperties.FIRST_PAGE, mCurrentPage);
    mCandidateAPIPropertiesMap.put(CandidateAPIProperties.CACHE, false);
    mDownloadCandidateListAsync = new DownloadCandidateListAsync(mCandidateAPIPropertiesMap);
    mDownloadCandidateListAsync.execute();
  }

  @Override protected void onPause() {
    super.onPause();
    if (mDownloadCandidateListAsync != null && !mDownloadCandidateListAsync.isCancelled()) {
      mDownloadCandidateListAsync.cancel(true);
    }
    if (mSearchCandidateTask != null && !mSearchCandidateTask.isCancelled()) {
      mSearchCandidateTask.cancel(true);
    }
    if (mTokenClass != null && !mTokenClass.isCancelled()) {
      mTokenClass.cancel(true);
    }
  }

  @Override public void onItemClick(View view, int position) {
    Intent goToCandiDetailIntent = new Intent();
    goToCandiDetailIntent.setClass(CandidateListActivity.this, CandidateDetailActivity.class);
    goToCandiDetailIntent.putExtra(CandidateDetailActivity.CANDIDATE_CONSTANT,
            (mSearchCandidates.size() > 0) ? mSearchCandidates.get(position) :
                    mCandidates.get(position));
    startActivity(goToCandiDetailIntent);
  }

  private void searchCandidateByName(String keyword) {
      mSearchCandidateTask = new SearchCandidateTask();
      mSearchCandidateTask.execute(keyword);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    return true;
  }

  class DownloadCandidateListAsync extends AsyncTask<Void, Void, List<Candidate>> {
    CandidateAPIPropertiesMap propertiesMap;

    public DownloadCandidateListAsync(CandidateAPIPropertiesMap propertiesMap) {
      this.propertiesMap = propertiesMap;
    }

    @Override protected List<Candidate> doInBackground(Void... voids) {
      propertiesMap.put(CandidateAPIProperties.WITH_PARTY, true);
      try {
        return getCandidateAPIHelper().getCandidates(propertiesMap);
      } catch (RetrofitError error) {
        error.printStackTrace();
      }
      return null;
    }

    @Override protected void onPostExecute(List<Candidate> candidates) {
      super.onPostExecute(candidates);
      if (candidates == null) {
        viewUtils.showProgress(mProgressView, mErrorView, true);
        mErrorText.setText(mmtext.processText(getString(R.string.SomethingWentWrong),
                mmtext.TEXT_UNICODE, true, true));
        return;
      }
      viewUtils.showProgress(mCandidateListRecyclerView, mProgressView, false);
      if (candidates.size() > 0) {
        if (mCurrentPage == 1) {
          mCandidates = candidates;
        } else {
          mCandidates.addAll(candidates);
        }
        mCandidateAdapter.setCandidates(mCandidates);
        mEndlessRecyclerViewAdapter.onDataReady(true);
        mCurrentPage++;
      } else if (candidates.size() == 0 && mCandidates.size() == 0) {
        mEndlessRecyclerViewAdapter.onDataReady(false);
        mErrorText.setText(getString(R.string.message_not_found));
        mErrorView.setVisibility(View.VISIBLE);
        mRetryBtn.setVisibility(View.GONE);
      } else {
        mEndlessRecyclerViewAdapter.onDataReady(false);
      }
    }
  }

  class SearchCandidateTask extends AsyncTask<String, Void, CandidateListReturnObject> {
    @Override
    protected CandidateListReturnObject doInBackground(String... strings) {
      if (strings[0] != null) {
        try {
          return getCandidateAPIHelper().searchCandidate(strings[0], mCandidateAPIPropertiesMap);
        } catch (RetrofitError error) {
          error.printStackTrace();
        }
      }
      return null;
    }

    @Override
    protected void onPostExecute(CandidateListReturnObject returnObject) {
      super.onPostExecute(returnObject);
      if (returnObject == null) {
        viewUtils.showProgress(mProgressView, mErrorView, true);
        mErrorText.setText(mmtext.processText(getString(R.string.SomethingWentWrong),
                mmtext.TEXT_UNICODE, true, true));
        return;
      }
      if (returnObject.getData().size() > 0) {
          mErrorView.setVisibility(View.GONE);
          viewUtils.showProgress(mCandidateListRecyclerView, mProgressView, false);
          mSearchCandidates.addAll(returnObject.getData());
          mCandidateAdapter.setCandidates(mSearchCandidates);
          mEndlessRecyclerViewAdapter.onDataReady(false);
      } else {
        viewUtils.showProgress(mProgressView, mErrorView, true);
        mCandidateListRecyclerView.setVisibility(View.GONE);
        mErrorText.setText(mmtext.processText(getString(R.string.search_not_found),
                mmtext.TEXT_UNICODE, true, true));
        mRetryBtn.setVisibility(View.GONE);
      }
    }
  }

  class TokenKeyGenerateClass extends AsyncTask<Void, Void, String> {

    @Override protected String doInBackground(Void... voids) {
      try {
        return mMaePaySohApiWrapper.getTokenKey();
      } catch (RetrofitError error) {
        error.printStackTrace();
      }
      return null;
    }

    @Override protected void onPostExecute(String s) {
      super.onPostExecute(s);
      if (s == null) {
        viewUtils.showProgress(mProgressView, mErrorView, true);
        mErrorText.setText(mmtext.processText(getString(R.string.SomethingWentWrong),
                mmtext.TEXT_UNICODE, true, true));
        return;
      }
      //mViewUtils.showProgress(mMainContent, mProgressBar, false);
      mMaePaySohApiWrapper.setTokenKey(s);
      PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
              .edit()
              .putString(Constants.API_KEY, s)
              .apply();
      if (mSearchName.isEmpty()) {
        downloadCandidateList();
      } else {
        searchCandidateByName(mSearchName);
      }
    }
  }
}
