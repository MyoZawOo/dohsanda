package com.dohsanda.dohsanda.ui;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dohsanda.dohsanda.Constants;
import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.adapters.EndlessRecyclerViewAdapter;
import com.dohsanda.dohsanda.adapters.PartyAdapter;
import com.dohsanda.dohsanda.utils.InternetUtils;
import com.dohsanda.dohsanda.utils.ViewUtils;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.PartyAPIHelper;
import org.maepaysoh.maepaysohsdk.db.PartyDao;
import org.maepaysoh.maepaysohsdk.models.Party;
import org.maepaysoh.maepaysohsdk.models.PartyListReturnObject;
import org.maepaysoh.maepaysohsdk.utils.PartyAPIProperties;
import org.maepaysoh.maepaysohsdk.utils.PartyAPIPropertiesMap;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PartyListActivity extends BaseActivity implements PartyAdapter.ClickInterface {
  MaePaySohApiWrapper maePaySohApiWrapper;
  private RecyclerView mPartyListRecyclerView;
  private ProgressBar mProgressView;
  private View mErrorView;
  private Button mRetryBtn;
  private TextView mErrorText;
  private List<Party> mParties = new ArrayList<>();
  private EndlessRecyclerViewAdapter mEndlessRecyclerViewAdapter;
  private PartyAdapter mPartyAdapter;
  private TokenKeyGenerateClass mTokenClass;

  private ViewUtils viewUtils;
  private PartyDao mPartyDao;
  private PartyAPIHelper mPartyAPIHelper;
  private DownloadPartyListAsync mDownloadPartyListAsync;
  private MenuItem mSearchMenu;
  private android.support.v7.widget.SearchView mSearchView;
  private PartyAPIPropertiesMap mPartyAPIPropertiesMap;
  private int mCurrentPage = 1;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_party_list);

    Toolbar mToolbar = (Toolbar) findViewById(R.id.party_list_toolbar);
    View mToolbarShadow = findViewById(R.id.party_list_toolbar_shadow);
    mErrorView = findViewById(R.id.party_list_error_view);
    mErrorText = (TextView) mErrorView.findViewById(R.id.error_view_error_text);
    mPartyListRecyclerView = (RecyclerView) findViewById(R.id.party_list_recycler_view);
    mProgressView = (ProgressBar) findViewById(R.id.party_list_progress_bar);
    mRetryBtn = (Button) mErrorView.findViewById(R.id.error_view_retry_btn);

    mProgressView.getIndeterminateDrawable()
        .setColorFilter(getResources().getColor(R.color.primary), PorterDuff.Mode.SRC_ATOP);

    mToolbar.setTitle(getString(R.string.PartyList));

    hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);

    setSupportActionBar(mToolbar);

    ActionBar mActionBar = getSupportActionBar();
    if (mActionBar != null) {
      // Showing Back Arrow  <-
      mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    viewUtils = new ViewUtils(this);

    // Show Progress on start
    viewUtils.showProgress(mPartyListRecyclerView, mProgressView, true);
    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    mPartyListRecyclerView.setLayoutManager(mLayoutManager);
    mPartyAdapter = new PartyAdapter();
    mPartyAdapter.setOnItemClickListener(PartyListActivity.this);
    mEndlessRecyclerViewAdapter = new EndlessRecyclerViewAdapter(this, mPartyAdapter,
            new EndlessRecyclerViewAdapter.RequestToLoadMoreListener() {
              @Override public void onLoadMoreRequested() {
                downloadPartyList();
              }
            });
    mPartyListRecyclerView.setAdapter(mEndlessRecyclerViewAdapter);
    maePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
    maePaySohApiWrapper.setFont(MaePaySohApiWrapper.FONT.unicode);
    mPartyAPIPropertiesMap = new PartyAPIPropertiesMap();
    mPartyDao = new PartyDao(this);
    if (InternetUtils.isNetworkAvailable(this)) {
      checkAndCreateToken();
    } else {
      loadFromCache();
    }

    mRetryBtn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        viewUtils.showProgress(mPartyListRecyclerView, mProgressView, true);
        if (InternetUtils.isNetworkAvailable(PartyListActivity.this)) {
          checkAndCreateToken();
        } else {
          loadFromCache();
        }
      }
    });

    mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == android.R.id.home) {
      onBackPressed();
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void onItemClick(View view, int position) {
    if (mParties.size() > 0) {
      Intent goToPartyDetailIntent = new Intent();
      goToPartyDetailIntent.setClass(PartyListActivity.this, PartyDetailActivity.class);
      goToPartyDetailIntent.putExtra(PartyDetailActivity.PARTY_CONSTANT, mParties.get(position));
      startActivity(goToPartyDetailIntent);
    }
  }

  private void checkAndCreateToken() {
    String apiKey = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
            .getString(Constants.API_KEY, "");
    if (apiKey.length() > 0) {
      maePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
      maePaySohApiWrapper.setTokenKey(apiKey);
      downloadPartyList();
    } else {
      mTokenClass = new TokenKeyGenerateClass();
      mTokenClass.execute();
    }
  }

  private void downloadPartyList() {
    mPartyAPIPropertiesMap.put(PartyAPIProperties.FIRST_PAGE, mCurrentPage);
    getPartyApiHelper().getPartiesAsync(mPartyAPIPropertiesMap, new Callback<PartyListReturnObject>() {
      @Override public void success(PartyListReturnObject returnObject, Response response) {
        // Hide Progress on success
        viewUtils.showProgress(mPartyListRecyclerView, mProgressView, false);
        switch (response.getStatus()) {
          case 200:
            if (returnObject.getData().size() > 0) {
              mParties.addAll(returnObject.getData());
              for (Party data : mParties) {
                try {
                  mPartyDao.createParty(data);
                } catch (SQLException e) {
                  e.printStackTrace();
                }
              }
              mPartyAdapter.setParties(mParties);
              mEndlessRecyclerViewAdapter.onDataReady(true);
              mCurrentPage++;
            } else {
              if (mCurrentPage == 1) {
                loadFromCache();
              } else {
                mEndlessRecyclerViewAdapter.onDataReady(false);
              }
            }
            break;
        }
      }

      @Override public void failure(RetrofitError error) {
        switch (error.getKind()) {
          case HTTP:
            org.maepaysoh.maepaysohsdk.models.Error mError =
                (org.maepaysoh.maepaysohsdk.models.Error) error.getBodyAs(Error.class);
            Toast.makeText(PartyListActivity.this, mError.getError().getMessage(),
                Toast.LENGTH_SHORT).show();
            break;
          case NETWORK:
            Toast.makeText(PartyListActivity.this, getString(R.string.PleaseCheckNetwork),
                Toast.LENGTH_SHORT).show();
            break;
          case CONVERSION:
            Toast.makeText(PartyListActivity.this, getString(R.string.SomethingWentWrong),
                Toast.LENGTH_SHORT).show();
        }

        // Hide Progress on failure too
        //  viewUtils.showProgress(mPartyListRecyclerView, mProgressView, false);
        //  mErrorView.setVisibility(View.VISIBLE);
        loadFromCache();
      }
    });
  }

  private void loadFromCache() {
    mParties = getPartyApiHelper().getPartiesFromCache();
    mEndlessRecyclerViewAdapter.onDataReady(false);
    viewUtils.showProgress(mPartyListRecyclerView, mProgressView, false);
    if (mParties != null && mParties.size() > 0) {
      mPartyAdapter.setParties(mParties);
    } else {
      mErrorView.setVisibility(View.VISIBLE);
    }
  }

  private PartyAPIHelper getPartyApiHelper() {
    if (mPartyAPIHelper == null) {
      mPartyAPIHelper = maePaySohApiWrapper.getPartyApiHelper();
    }
    return mPartyAPIHelper;
  }

  private void downloadListSync() {
    mDownloadPartyListAsync = new DownloadPartyListAsync();
    mDownloadPartyListAsync.execute();
  }

  @Override protected void onPause() {
    super.onPause();
    if (mDownloadPartyListAsync != null) {
      mDownloadPartyListAsync.cancel(true);
    }
    if (mTokenClass != null && !mTokenClass.isCancelled()) {
      mTokenClass.cancel(true);
    }
  }

  class DownloadPartyListAsync extends AsyncTask<Void, Void, List<Party>> {
    @Override protected void onPreExecute() {
      super.onPreExecute();
      mErrorView.setVisibility(View.GONE);
    }

    @Override protected List<Party> doInBackground(Void... voids) {
      try {
        mPartyAPIPropertiesMap.put(PartyAPIProperties.FIRST_PAGE, mCurrentPage);
        return getPartyApiHelper().getParties(mPartyAPIPropertiesMap);
      } catch (RetrofitError error) {
        error.printStackTrace();
      }
      return null;
    }

    @Override protected void onPostExecute(List<Party> parties) {
      if (parties == null) {
        viewUtils.showProgress(mProgressView, mErrorView, true);
        mErrorText.setText(mmtext.processText(getString(R.string.SomethingWentWrong),
                mmtext.TEXT_UNICODE, true, true));
        return;
      }

      viewUtils.showProgress(mPartyListRecyclerView, mProgressView, false);
      if (parties.size() > 0) {
        mParties = parties;
        mPartyAdapter.setParties(mParties);
        mCurrentPage++;
      } else {
        Toast.makeText(PartyListActivity.this, getString(R.string.PleaseCheckNetwork),
            Toast.LENGTH_SHORT).show();
        loadFromCache();
      }
      super.onPostExecute(parties);
    }
  }

  class TokenKeyGenerateClass extends AsyncTask<Void, Void, String> {

    @Override protected String doInBackground(Void... voids) {
      try {
        return maePaySohApiWrapper.getTokenKey();
      } catch (RetrofitError error) {
        error.printStackTrace();
      }
      return null;
    }

    @Override protected void onPostExecute(String s) {
      super.onPostExecute(s);
      if (s == null) {
        viewUtils.showProgress(mProgressView, mErrorView, true);
        mErrorText.setText(mmtext.processText(getString(R.string.SomethingWentWrong),
                mmtext.TEXT_UNICODE, true, true));
        return;
      }
      //mViewUtils.showProgress(mMainContent, mProgressBar, false);
      maePaySohApiWrapper.setTokenKey(s);
      PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
              .edit()
              .putString(Constants.API_KEY, s)
              .apply();
      downloadListSync();
    }
  }
}

