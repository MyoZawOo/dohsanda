package com.dohsanda.dohsanda.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.adapters.CandidateAdapter;
import com.dohsanda.dohsanda.utils.InternetUtils;
import com.dohsanda.dohsanda.utils.TextUtils;
import com.dohsanda.dohsanda.utils.TimeUtils;

import org.maepaysoh.maepaysohsdk.CandidateAPIHelper;
import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.StateAPIHelper;
import org.maepaysoh.maepaysohsdk.models.Candidate;
import org.maepaysoh.maepaysohsdk.models.Constituency;
import org.maepaysoh.maepaysohsdk.utils.CandidateAPIProperties;
import org.maepaysoh.maepaysohsdk.utils.CandidateAPIPropertiesMap;

import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;

import static com.dohsanda.dohsanda.utils.TextUtils.checkDataIsProvided;

/**
 * Created by yemyatthu on 8/5/15.
 */
public class CandidateDetailActivity extends BaseActivity
        implements CandidateAdapter.ClickInterface {

  public static final String CANDIDATE_CONSTANT =
      "org.maepaesoh.maepaesoh.ui.CandidateDetailActivity";

  // Ui elements
  private Toolbar mToolbar;
  private View mToolbarShadow;

  private CandidateAPIHelper mCandidateAPIHelper;
  private StateAPIHelper mStateAPIHelper;
  private List<Candidate> mCandidates = new ArrayList<>();
  private DownloadCandidateListAsync mDownloadCandidateListAsync;

  // Ui elements
  private TextView mCandidateName;
  private TextView mLegislature;
  private TextView mNationalId;
  private TextView mBirthDate;
  private TextView mEducation;
  private TextView mOccupation;
  private TextView mReligion;
  private TextView mWardVillage;
  private TextView mConstituency;
  private TextView mParty;
  private TextView mMotherName;
  private TextView mFatherName;
  private TextView mEthnicity;
  private ImageView mCandidateImage;
  private TextView mPrevCandidate;
  private TextView mCandidateParty;
  private Button mCompareCandidateBtn;
  private Candidate mCandidate;
  private AlertDialog mDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_candidate_detail);
    mToolbar = (Toolbar) findViewById(R.id.candidate_detail_toolbar);
    mToolbarShadow = findViewById(R.id.candidate_detail_toolbar_shadow);
    hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);
    mToolbar.setBackgroundColor(getResources().getColor(R.color.candidate));
    mToolbar.setTitle(getString(R.string.CandidateDetailTitle));
    setStatusBarColor(R.color.dark_candidate);

    setSupportActionBar(mToolbar);
    ActionBar mActionBar = getSupportActionBar();
    if (mActionBar != null) {
      // Showing Back Arrow  <-
      mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    mCandidate = (Candidate) getIntent().getSerializableExtra(CANDIDATE_CONSTANT);

    MaePaySohApiWrapper maePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
    mCandidateAPIHelper = maePaySohApiWrapper.getCandidateApiHelper();
    mStateAPIHelper = maePaySohApiWrapper.getStateApiHelper();

    mCandidateName = (TextView) findViewById(R.id.candidate_name);
    mCandidateImage = (ImageView) findViewById(R.id.candidate_image);
    mLegislature = (TextView) findViewById(R.id.candidate_legislature);
    mNationalId = (TextView) findViewById(R.id.candidate_national_id);
    mBirthDate = (TextView) findViewById(R.id.candidate_birth_date);
    mEducation = (TextView) findViewById(R.id.candidate_education);
    mOccupation = (TextView) findViewById(R.id.candidate_occupation);
    mReligion = (TextView) findViewById(R.id.candidate_religion);
    mWardVillage = (TextView) findViewById(R.id.candidate_ward_village);
    mConstituency = (TextView) findViewById(R.id.candidate_constituency);
    mMotherName = (TextView) findViewById(R.id.candidate_mother);
    mFatherName = (TextView) findViewById(R.id.candidate_father);
    mEthnicity = (TextView) findViewById(R.id.candidate_ethnicity);
    mPrevCandidate = (TextView) findViewById(R.id.candidate_prev);
    mCandidateParty = (TextView) findViewById(R.id.candidate_party);
    mCompareCandidateBtn = (Button) findViewById(R.id.compare_candidate_btn);

    setupCandidateData();

    mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
  }

  private void setupCandidateData() {
    if (mCandidate != null) {
      mCandidateName.setText(mCandidate.getName());
      Glide.with(this)
              .load(mCandidate.getPhotoUrl())
              .diskCacheStrategy(DiskCacheStrategy.ALL)
              .into(mCandidateImage);
      mLegislature.setText(mCandidate.getLegislature());
      mNationalId.setText(checkDataIsProvided(mCandidate.getNationalId(), this));
      String birthday = getString(R.string.label_no_data);
      String timestamp = String.valueOf(mCandidate.getBirthdate());
      if (! timestamp.isEmpty()) {
        birthday = TextUtils.convertToMM(TimeUtils.getDateFromUnixTimeStamp(
                Long.parseLong(timestamp)));
      }
      mBirthDate.setText(birthday);
      mReligion.setText(checkDataIsProvided(mCandidate.getReligion(), this));
      mConstituency.setText(formatConstituency(mCandidate.getConstituency()));
      mWardVillage.setText(checkDataIsProvided(mCandidate.getWardVillage(), this));
      mMotherName.setText(checkDataIsProvided(mCandidate.getMother().getName(), this));
      mFatherName.setText(checkDataIsProvided(mCandidate.getFather().getName(), this));
      mEthnicity.setText(checkDataIsProvided(mCandidate.getEthnicity(), this));
      mOccupation.setText(checkDataIsProvided(mCandidate.getOccupation(), this));
      mEducation.setText(checkDataIsProvided(mCandidate.getEducation(), this));
      if (mCandidate.getMpid() == null) {
        mPrevCandidate.setText(getString(R.string.CandidatePreviousNo));
      } else {
        mPrevCandidate.setText(getString(R.string.CandidatePreviousYes));
      }

      String partyName = (mCandidate.getParty() == null) ? mCandidate.getPartyName() :
              mCandidate.getParty().getPartyName();
      if (partyName == null || partyName.isEmpty()) {
        partyName = getString(R.string.label_single_candidate);
      } else {
        mCandidateParty.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent partyIntent = new Intent(CandidateDetailActivity.this, PartyDetailActivity.class);
            partyIntent.putExtra(PartyDetailActivity.PARTY_CONSTANT, mCandidate.getParty());
            startActivity(partyIntent);
          }
        });
      }
      mCandidateParty.setText(partyName);

      mCompareCandidateBtn.setVisibility(View.VISIBLE);
      mCompareCandidateBtn.setEnabled(false);
      mCompareCandidateBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          showCandidateListDialog();
        }
      });

      if (InternetUtils.isNetworkAvailable(this)) {
        downloadCandidateList();
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  public void onItemClick(View view, int position) {
    Intent intent = new Intent(this, CandidateComparisonActivity.class);
    intent.putExtra(CandidateComparisonActivity.EXTRA_CADIDATE_ONE, mCandidate);
    intent.putExtra(CandidateComparisonActivity.EXTRA_CADIDATE_TWO, mCandidates.get(position));
    mDialog.dismiss();
    startActivity(intent);
  }

  @Override
  public void onPause() {
    super.onPause();
    if (mDownloadCandidateListAsync != null) {
      mDownloadCandidateListAsync.cancel(true);
    }
  }

  private String formatConstituency(Constituency constituency) {
    String candidateLabel = getString(R.string.CandidateConstituencyNo);

    if (constituency.getaMPCode() != null) {
      String ampCodeEng = constituency.getaMPCode();
      ampCodeEng = ampCodeEng.substring(ampCodeEng.length() - 2, ampCodeEng.length());
      String ampCodeMM = TextUtils.convertToMM(String.valueOf(Integer.parseInt(ampCodeEng)));
      return mStateAPIHelper.getStateById(constituency.getsTPCode()).getName()
              + " " + candidateLabel + " (" + ampCodeMM + ")";
    }
    return constituency.getName();
  }

  private void downloadCandidateList() {
    mDownloadCandidateListAsync = new DownloadCandidateListAsync(mCandidate);
    mDownloadCandidateListAsync.execute();
  }

  private void showCandidateListDialog() {
    String titleTemplate = "%s တွင် %s နှင့်အတူ ဝင်ရောက်ယှဉ်ပြိုင်မည့် %sကိုယ်စားလှယ်လောင်းများ";
    Constituency constituency = mCandidate.getConstituency();
    String constituencyText;
    String candidateLabel = getString(R.string.CandidateConstituencyNo);

    if (constituency.getaMPCode() != null) {
      String ampCodeEng = constituency.getaMPCode();
      ampCodeEng = ampCodeEng.substring(ampCodeEng.length() - 2, ampCodeEng.length());
      String ampCodeMM = TextUtils.convertToMM(String.valueOf(Integer.parseInt(ampCodeEng)));
      constituencyText = mStateAPIHelper.getStateById(constituency.getsTPCode()).getName()
              + " " + candidateLabel + " (" + ampCodeMM + ")";
    } else {
      constituencyText = constituency.getName();
    }

    View dialog = LayoutInflater.from(this).inflate(R.layout.dialog_candidate_competitor, null);
    TextView titleView = (TextView) dialog.findViewById(R.id.title);
    titleView.setText(String.format(titleTemplate, constituencyText, mCandidate.getName(),
            mCandidate.getLegislature()));
    mmtext.prepareView(this, titleView, mmtext.TEXT_UNICODE, true, true);
    RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.competitor_list);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    CandidateAdapter candidateAdapter = new CandidateAdapter();
    candidateAdapter.setOnItemClickListener(this);
    candidateAdapter.setCandidates(mCandidates);
    recyclerView.setAdapter(candidateAdapter);

    mDialog = new AlertDialog.Builder(this)
            .setView(dialog)
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
              }
            }).show();

    Button cancelButton = mDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
    mmtext.prepareView(this, cancelButton, mmtext.TEXT_UNICODE, true, true);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_candidate_detail, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private class DownloadCandidateListAsync extends AsyncTask<Void, Void, List<Candidate>> {
    private Candidate candidate;

    public DownloadCandidateListAsync(Candidate candidate) {
      this.candidate = candidate;
    }

    @Override protected List<Candidate> doInBackground(Void... voids) {
      CandidateAPIPropertiesMap propertiesMap = new CandidateAPIPropertiesMap();
      propertiesMap.put(CandidateAPIProperties.WITH_PARTY, true);
      propertiesMap.put(CandidateAPIProperties.CACHE, false);
      propertiesMap.put(CandidateAPIProperties.PER_PAGE, 30);

      return mCandidateAPIHelper.getCandidatesByConstituency(candidate,
              propertiesMap);
    }

    @Override protected void onPostExecute(List<Candidate> candidates) {
      super.onPostExecute(candidates);
      if (candidates.size() > 0) {
        for (Candidate current : candidates) {
          if (! current.getId().equals(candidate.getId())) {
            mCandidates.add(current);
          }
        }
        mCompareCandidateBtn.setText(mmtext.processText(
                getString(R.string.label_compare_candidate),
                mmtext.TEXT_UNICODE, true, true));
        mCompareCandidateBtn.setEnabled(true);
      }
    }
  }
}
