package com.dohsanda.dohsanda.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dohsanda.dohsanda.Constants;
import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.StateAPIHelper;
import org.maepaysoh.maepaysohsdk.TownshipAPIHelper;
import org.maepaysoh.maepaysohsdk.models.State;
import org.maepaysoh.maepaysohsdk.models.Township;

import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;

public class SearchActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    public static final String EXTRA_SEARCH_NAME =
            "org.maepaesoh.maepaesoh.ui.SearchActivity.EXTRA_SEARCH_NAME";
    public static final String EXTRA_SEARCH_STATE =
            "org.maepaesoh.maepaesoh.ui.SearchActivity.EXTRA_SEARCH_STATE";
    public static final String EXTRA_SEARCH_TOWNSHIP =
            "org.maepaesoh.maepaesoh.ui.SearchActivity.EXTRA_SEARCH_TOWNSHIP";
    public static final String EXTRA_SEARCH_LEGISLATURE =
            "org.maepaesoh.maepaesoh.ui.SearchActivity.EXTRA_SEARCH_LEGISLATURE";
    public static final String EXTRA_SEARCH_TITLE =
            "org.maepaesoh.maepaesoh.ui.SearchActivity.EXTRA_SEARCH_TITLE";

    private Toolbar mToolbar;
    private View mToolbarShadow;
    private MaePaySohApiWrapper mMaePaySohApiWrapper;
    private TownshipAPIHelper mTownshipAPIHelper;

    private View mNameContainer;
    private View mStateContainer;

    private Spinner mStateSpinner;
    private Spinner mLegislatureSpinner;
    private Spinner mTownshipSpinner;
    private LinearLayout mTownshipContainer;

    private EditText mCandidateName;

    private Button mSearchBtn;

    private String mSelectedState;
    private String mSelectedLegislature;
    private String mSelectedTownship;

    private String mSelectedLegislatureName;
    private String mSelectedStateName;
    private String mSelectedTownshipName;
    private int mSearchType = 0;

    private boolean mIsUpperHouse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mToolbar = (Toolbar) findViewById(R.id.search_detail_toolbar);
        mToolbarShadow = findViewById(R.id.search_detail_toolbar_shadow);
        hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);
        mToolbar.setTitle(getString(R.string.title_activity_search));
        mToolbar.setBackgroundColor(getResources().getColor(R.color.candidate));
        mmtext.prepareViewGroup(this, mToolbar, mmtext.TEXT_UNICODE, true, true);
        setStatusBarColor(R.color.dark_candidate);

        setSupportActionBar(mToolbar);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            // Showing Back Arrow  <-
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        mMaePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
        mTownshipAPIHelper = mMaePaySohApiWrapper.getTownshipApiHelper();

        mNameContainer = findViewById(R.id.name_container);
        mStateContainer = findViewById(R.id.state_container);
        mmtext.prepareViewGroup(this, ((ViewGroup) mStateContainer), mmtext.TEXT_UNICODE, true, true);

        RadioGroup mTypeRadioGroup = (RadioGroup) findViewById(R.id.type_radio);
        mTypeRadioGroup.setOnCheckedChangeListener(this);
        mmtext.prepareViewGroup(this, mTypeRadioGroup, mmtext.TEXT_UNICODE, true, true);

        TextView nameLabel = (TextView) findViewById(R.id.name_label);
        mmtext.prepareView(this, nameLabel, mmtext.TEXT_UNICODE, true, true);
        mCandidateName = (EditText) findViewById(R.id.candidate_name);

        mTownshipContainer = (LinearLayout) findViewById(R.id.township_container);
        mTownshipSpinner = (Spinner) findViewById(R.id.township_spinner);
        mLegislatureSpinner = (Spinner) findViewById(R.id.legislature_spinner);
        mSearchBtn = (Button) findViewById(R.id.search_btn);

        //mSearchBtn.setBackgroundColor(getResources().getColor(R.color.candidate));

        mmtext.prepareView(this, mSearchBtn, mmtext.TEXT_UNICODE, true, true);

        setupStateSpinner();
        setupLegislatureSpinner();
        setupTownshipSpinner(null);
        setupSearchBtn();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == R.id.type_name) {
            mStateContainer.setVisibility(View.GONE);
            mNameContainer.setVisibility(View.VISIBLE);
            mSearchType = 1;
        } else {
            mStateContainer.setVisibility(View.VISIBLE);
            mNameContainer.setVisibility(View.GONE);
            mSearchType = 0;
        }
    }

    private void setupStateSpinner() {
        mStateSpinner = (Spinner) findViewById(R.id.state_spinner);
        StateAPIHelper stateAPIHelper = mMaePaySohApiWrapper.getStateApiHelper();
        final List<State> states = stateAPIHelper.getAllStatesData();
        final List<String> stateList = new ArrayList<>();
        String prefix = mmtext.processText(getResources().getString(R.string.label_state_prefix),
                mmtext.TEXT_UNICODE, true, true);
        stateList.add(prefix);
        for (State state : states) {
            stateList.add(mmtext.processText(state.getName(), mmtext.TEXT_UNICODE, true, true));
        }
        ArrayAdapter mStateAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, R.id.item_spinner,
                stateList);
        mStateSpinner.setAdapter(mStateAdapter);
        mStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedStateName = stateList.get(i);
                if (i > 0) {
                    State state = states.get(--i);
                    mSelectedState = state.getId();
                    setupTownshipSpinner(mSelectedState);
                    if (! mIsUpperHouse) {
                        mTownshipSpinner.setEnabled(true);
                    }
                } else {
                    mSelectedState = null;
                    mTownshipSpinner.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mSelectedState = null;
                mSelectedStateName = stateList.get(0);
            }
        });
    }

    private void setupTownshipSpinner(String stateCode) {
        final List<Township> townships = new ArrayList<>();
        if (stateCode != null) {
            townships.addAll(mTownshipAPIHelper.getTownshipsByStateCode(stateCode));
        }

        final List<String> townshipList = new ArrayList<>();
        if (townships.size() > 0) {
            String prefix = mmtext.processText(getResources().getString(R.string.label_township_prefix),
                    mmtext.TEXT_UNICODE, true, true);
            townshipList.add(prefix);
            for (Township township : townships) {
                townshipList.add(mmtext.processText(township.getName(), mmtext.TEXT_UNICODE, true, true));
            }
        } else {
            String notice = mmtext.processText(getResources().getString(R.string.label_township_notice),
                    mmtext.TEXT_UNICODE, true, true);
            townshipList.add(notice);
        }
        ArrayAdapter mTownshipAdapter = new ArrayAdapter<>(this, R.layout.item_spinner,
                R.id.item_spinner, townshipList);
        mTownshipSpinner.setAdapter(mTownshipAdapter);
        mTownshipSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    mSelectedTownshipName = townshipList.get(i) + "မဲဆန္ဒနယ်";
                    mSelectedTownship = townships.get(--i).getId();
                } else {
                    mSelectedTownshipName = "";
                    mSelectedTownship = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mSelectedTownship = null;
                mSelectedTownshipName = (mSelectedState != null) ? townshipList.get(0) : "";
            }
        });
    }

    private void setupLegislatureSpinner() {
        final List<String> legislatureList = new ArrayList<>();
        legislatureList.add(mmtext.processText(getResources().getString(
                R.string.label_legislature_prefix), mmtext.TEXT_UNICODE, true, true));
        int len = Constants.SEARCH_LEGISLATURE.length;
        for (int i = 0; i < len; i++) {
            legislatureList.add(mmtext.processText(Constants.SEARCH_LEGISLATURE[i],
                    mmtext.TEXT_UNICODE, true, true));
        }
        ArrayAdapter mLegislatureAdapter = new ArrayAdapter<>(this, R.layout.item_spinner,
                R.id.item_spinner, legislatureList);
        mLegislatureSpinner.setAdapter(mLegislatureAdapter);
        mLegislatureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    mIsUpperHouse = true;
                    mTownshipSpinner.setEnabled(false);
                    setupTownshipSpinner(null);
                    mTownshipContainer.setVisibility(View.GONE);
                } else {
                    mIsUpperHouse = false;
                    if (mSelectedState != null) {
                        mTownshipSpinner.setEnabled(true);
                    }
                    setupTownshipSpinner(mSelectedState);
                    mTownshipContainer.setVisibility(View.VISIBLE);
                }
                if (i > 0) {
                    mSelectedLegislatureName = legislatureList.get(i) + "ကိုယ်စားလှယ်အဖြစ်";
                    mSelectedLegislature = Constants.SEARCH_LEGISLATURE_ENG[--i];
                } else {
                    mSelectedLegislature = null;
                    mSelectedLegislatureName = "";
                 }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mSelectedLegislature = null;
                mSelectedLegislatureName = "";
            }
        });
    }

    private void setupSearchBtn() {
        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SearchActivity.this, CandidateListActivity.class);

                if (mSearchType == 1) {
                    if (mCandidateName.getText().toString().isEmpty()) {
                        showErrorAlertDialog(formatRequiredErrorMessage(getString(R.string.label_name)));
                        return;
                    }
                    String name = checkAndConvertZg(mCandidateName.getText().toString());
                    String nameTemplate = "အမည်တွင် '%s' ပါဝင်သော ကိုယ်စားလှယ်လောင်းများ";
                    String text = String.format(nameTemplate, name);
                    intent.putExtra(EXTRA_SEARCH_NAME, name);
                    intent.putExtra(EXTRA_SEARCH_TITLE, text);
                } else {
                    String stateTemplate = "%s %sမှ %s ဝင်ရောက်ယှဉ်ပြိုင်မည့် ကိုယ်စားလှယ်လောင်းများ";
                    String text = String.format(stateTemplate, mSelectedStateName, mSelectedTownshipName,
                            mSelectedLegislatureName);
                    if (mSelectedState != null) {
                        intent.putExtra(EXTRA_SEARCH_STATE, mSelectedState);
                    }
                    if (mSelectedTownship != null) {
                        intent.putExtra(EXTRA_SEARCH_TOWNSHIP, mSelectedTownship);
                    }
                    if (mSelectedLegislature != null) {
                        intent.putExtra(EXTRA_SEARCH_LEGISLATURE, mSelectedLegislature);
                    }
                    intent.putExtra(EXTRA_SEARCH_TITLE, text);
                }
                startActivity(intent);
            }
        });
    }


}
