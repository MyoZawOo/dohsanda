package com.dohsanda.dohsanda.ui;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.utils.InternetUtils;
import com.dohsanda.dohsanda.utils.TextUtils;
import com.dohsanda.dohsanda.utils.ViewUtils;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.api.CancelableCallback;
import org.maepaysoh.maepaysohsdk.models.Voter;
import org.maepaysoh.maepaysohsdk.utils.VoterAPIProperties;
import org.maepaysoh.maepaysohsdk.utils.VoterAPIPropertiesMap;

import java.util.Calendar;

import mm.technomation.tmmtextutilities.mmtext;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CheckVoterActivity extends BaseActivity
        implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,
        RadioGroup.OnCheckedChangeListener {

    private static final int MAX_YEAR = 1997;
    private static final int MAX_MONTH = 11; // starting count at 0
    private static final int MAX_DAY = 8;

    private int mYear;
    private int mMonth;
    private int mDay;

    private View mCheckerContainer;
    private View mVoterInfoContainer;
    private View mDobContainer;
    private EditText mName;
    private TextView mDob;
    private RadioGroup mTypeRadioGroup;
    private View mNRCContainer;
    private EditText mNRCPrefix;
    private EditText mNRCCode;
    private EditText mNRCNo;
    private EditText mFatherName;
    private Button mSearchBtn;

    private TextView mVoterName;
    private TextView mVoterDob;
    private TextView mVoterNrc;
    private TextView mVoterFather;
    private TextView mVoterState;
    private TextView mVoterDistrict;
    private TextView mVoterTownship;
    private TextView mVoterWard;

    private View mErrorView;
    private TextView mErrorText;
    private Button mRetryButton;

    private ProgressBar mProgressBar;
    private int type = 0;
    private boolean isChecked = false;

    private MaePaySohApiWrapper mMaePaySohApiWrapper;
    private VoterAPIPropertiesMap mVoterAPIPropertiesMap;
    private ViewUtils mViewUtils;
    private CancelableCallback mCheckVoterCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_voter);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.voter_toolbar);
        View mToolbarShadow = findViewById(R.id.voter_toolbar_shadow);
        mToolbar.setTitle(getString(R.string.title_activity_check_voter));
        mToolbar.setBackgroundColor(getResources().getColor(R.color.check_voter_list));
        setStatusBarColor(R.color.dark_check_voter_list);
        mmtext.prepareViewGroup(this, ((ViewGroup) mToolbar), mmtext.TEXT_UNICODE, true, true);
        hideToolBarShadowForLollipop(mToolbar, mToolbarShadow);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mMaePaySohApiWrapper = MaePaySoh.getMaePaySohWrapper();
        mVoterAPIPropertiesMap = new VoterAPIPropertiesMap();
        mViewUtils = new ViewUtils(this);

        mCheckerContainer = findViewById(R.id.checker_container);
        mVoterInfoContainer = findViewById(R.id.voter_info_container);
        mmtext.prepareViewGroup(this, ((ViewGroup) mVoterInfoContainer),
                mmtext.TEXT_UNICODE, true, true);

        mDobContainer = findViewById(R.id.dob_container);
        View dobWrapper = findViewById(R.id.dob_wrapper);
        mNRCContainer = findViewById(R.id.nrc_container);
        mmtext.prepareViewGroup(this, ((ViewGroup) dobWrapper), mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareViewGroup(this, ((ViewGroup) mNRCContainer), mmtext.TEXT_UNICODE, true, true);
        mTypeRadioGroup = (RadioGroup) findViewById(R.id.type_radio);
        mmtext.prepareViewGroup(this, ((ViewGroup) mTypeRadioGroup), mmtext.TEXT_UNICODE, true, true);
        mTypeRadioGroup.setOnCheckedChangeListener(this);

        mNRCCode = (EditText) findViewById(R.id.nrc_code);
        mNRCPrefix = (EditText) findViewById(R.id.nrc_prefix);
        mNRCNo = (EditText) findViewById(R.id.nrc_no);

        mYear = MAX_YEAR;
        mMonth = MAX_MONTH;
        mDay = MAX_DAY;

        mDob = (TextView) findViewById(R.id.dob);
        mDob.setText(TextUtils.convertToMM(formatDateString()));
        mDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCalendar();
            }
        });

        mFatherName = (EditText) findViewById(R.id.father_name);
        TextView fatherNameLabel = (TextView) findViewById(R.id.father_name_label);
        mmtext.prepareView(this, fatherNameLabel, mmtext.TEXT_UNICODE, true, true);

        mSearchBtn = (Button) findViewById(R.id.check_btn);
        mSearchBtn.setBackgroundColor(getResources().getColor(R.color.check_voter_list));
        mmtext.prepareView(this, mSearchBtn, mmtext.TEXT_UNICODE, true, true);
        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkVoter();
            }
        });

        mVoterName = (TextView) findViewById(R.id.voter_name);
        mVoterDob = (TextView) findViewById(R.id.voter_dob);
        mVoterNrc = (TextView) findViewById(R.id.voter_nrc);
        mVoterFather = (TextView) findViewById(R.id.voter_father);
        mVoterState = (TextView) findViewById(R.id.voter_state);
        mVoterDistrict = (TextView) findViewById(R.id.voter_district);
        mVoterTownship = (TextView) findViewById(R.id.voter_township);
        mVoterWard = (TextView) findViewById(R.id.voter_ward);

        mProgressBar = (ProgressBar) findViewById(R.id.voter_progress_bar);
        mProgressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.primary), PorterDuff.Mode.SRC_ATOP);

        mName = (EditText) findViewById(R.id.name);
        mmtext.prepareView(this, findViewById(R.id.name_label), mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, findViewById(R.id.disclaimer), mmtext.TEXT_UNICODE, true, true);

        mErrorView = findViewById(R.id.voter_error_view);
        mErrorText = (TextView) mErrorView.findViewById(R.id.error_view_error_text);
        mRetryButton = (Button) mErrorView.findViewById(R.id.error_view_retry_btn);
        mmtext.prepareViewGroup(this, ((ViewGroup) mErrorView), mmtext.TEXT_UNICODE, true, true);
        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkVoter();
            }
        });
    }

    private String formatDateString() {
        return mYear + "-" + ((mMonth < 10) ? "0" + mMonth : mMonth) + "-"
                + ((mDay < 10) ? "0" + mDay : mDay);
    }

    private void showCalendar() {
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog mDatePicker =
                com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                        this,
                        mYear,
                        mMonth-1,
                        mDay
                );
        mDatePicker.setAccentColor(R.color.check_voter_list);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(MAX_YEAR, MAX_MONTH-1, MAX_DAY);
        mDatePicker.setMaxDate(maxDate);
        mDatePicker.show(getFragmentManager(), "Calendar");
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog,
                          int year, int month, int date) {
        mYear = year;
        mMonth = month + 1;
        mDay = date;
        mDob.setText(TextUtils.convertToMM(formatDateString()));
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == R.id.type_nrc) {
            mDobContainer.setVisibility(View.GONE);
            mNRCContainer.setVisibility(View.VISIBLE);
            type = 1;
        } else {
            mDobContainer.setVisibility(View.VISIBLE);
            mNRCContainer.setVisibility(View.GONE);
            type = 0;
        }
    }

    private boolean validateInfo() {
        if (mName.getText().toString().isEmpty()) {
            showErrorAlertDialog(formatRequiredErrorMessage(getString(R.string.voter_name)));
            return false;
        }
        if (type == 1) {
            if (mNRCPrefix.getText().toString().isEmpty() && mNRCCode.getText().toString().isEmpty()
                    && mNRCNo.getText().toString().isEmpty()) {
                showErrorAlertDialog(formatRequiredErrorMessage(getString(R.string.voter_nrc)));
                return false;
            }
            mVoterAPIPropertiesMap.put(VoterAPIProperties.NRC_NO, mNRCPrefix.getText() +
                "/" + mNRCCode.getText() + "(နိုင်)" + mNRCNo.getText());
        } else {
            if (mDob.getText().toString().isEmpty()) {
                showErrorAlertDialog(formatRequiredErrorMessage(getString(R.string.voter_dob)));
                return false;
            }

            if (mFatherName.getText().toString().isEmpty()) {
                showErrorAlertDialog(formatRequiredErrorMessage(getString(R.string.CandidateFather)));
                return false;
            }

            String fatherName = checkAndConvertZg(mFatherName.getText().toString());

            mVoterAPIPropertiesMap.put(VoterAPIProperties.DOB, formatDateString());
            mVoterAPIPropertiesMap.put(VoterAPIProperties.FATHER_NAME, fatherName);
        }
        return true;
    }

    private void checkVoter() {
        if (! validateInfo()) {
            return;
        }

        if (! InternetUtils.isNetworkAvailable(CheckVoterActivity.this)) {
            mViewUtils.showProgress(mCheckerContainer, mErrorView, true);
            mErrorText.setText(mmtext.processText(getString(R.string.PleaseCheckNetwork),
                    mmtext.TEXT_UNICODE, true, true));
            return;
        }

        mErrorView.setVisibility(View.GONE);

        String name = checkAndConvertZg(mName.getText().toString());
        mViewUtils.showProgress(mCheckerContainer, mProgressBar, true);
        mCheckVoterCallback = new CancelableCallback<Voter>() {
            @Override
            protected void onSuccess(Voter voter, Response response) {
                setupVoterInfo(voter);
            }

            @Override
            protected void onFailure(RetrofitError error) {
                mViewUtils.showProgress(mCheckerContainer, mProgressBar, false);
                if (error.getResponse().getStatus() == 201) {
                    showErrorAlertDialog(mmtext.processText(
                            getString(R.string.message_voter_not_found),
                            mmtext.TEXT_UNICODE,
                            true, true
                    ));
                } else {
                    showErrorAlertDialog(mmtext.processText(
                            getString(R.string.SomethingWentWrong),
                            mmtext.TEXT_UNICODE,
                            true, true
                    ));
                }
            }
        };
        mMaePaySohApiWrapper.getVoterApiHelper()
                .searchVoter(name, mVoterAPIPropertiesMap, mCheckVoterCallback);
    }

    private void setupVoterInfo(Voter voter) {
        mVoterName.setText(voter.getVoterName());
        mVoterDob.setText(voter.getDateOfBirth());
        mVoterNrc.setText(voter.getNrcno());
        mVoterFather.setText(voter.getFatherName());
        mVoterState.setText(voter.getState());
        mVoterDistrict.setText(voter.getDistrict());
        mVoterTownship.setText(voter.getTownship());
        mVoterWard.setText(voter.getVillage());
        mmtext.prepareViewGroup(this, ((ViewGroup) mVoterInfoContainer),
                mmtext.TEXT_UNICODE, true, true);
        mViewUtils.showProgress(mVoterInfoContainer, mProgressBar, false);
        mErrorView.setVisibility(View.GONE);
        isChecked = true;
    }


    @Override
    public void onBackPressed() {
        if (isChecked) {
            mViewUtils.showProgress(mCheckerContainer, mVoterInfoContainer, false);
            isChecked = false;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCheckVoterCallback != null) {
            mCheckVoterCallback.cancel(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_check_voter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
