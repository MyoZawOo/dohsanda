package com.dohsanda.dohsanda;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dohsanda.dohsanda.ui.BaseActivity;
import com.dohsanda.dohsanda.ui.CheckVoterActivity;
import com.dohsanda.dohsanda.ui.PartyListActivity;
import com.dohsanda.dohsanda.ui.SearchActivity;
import com.dohsanda.dohsanda.utils.TimeUtils;
import com.dohsanda.education.KnowledgeActivity;
import com.dohsanda.game.GameActivity;
import com.dohsanda.intro.IntroActivity;
import com.dohsanda.intro.PrefConstants;
import com.dohsanda.intro.SAppUtil;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by myozawoo on 9/21/15.
 */
public class MainActivity extends BaseActivity {

    private ImageView btnCandidate;
    private ImageView btnKnowledge;
    private ImageView btnParty;
    private ImageView btnGame;
    private ImageView btnSearch;
    private TextView txtTips;
    private Toolbar mToolbar;


    private Handler mHandler;


    private ImageView imgKnowledge, imgCandidate, imgParty, imgGame;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        checkShowTutorial();
        super.onCreate(savedInstanceState);

//        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 1);

        setContentView(R.layout.activity_home);

        mToolbar = (Toolbar) findViewById(R.id.home_toolbar);
        mToolbar.setTitle(getString(R.string.app_name_mm));
        mToolbar.setLogo(R.drawable.home_icon_drawable);

        setSupportActionBar(mToolbar);

        ActionBar mActionBar = getSupportActionBar();
        //mActionBar.setIcon(R.drawable.ic_action_bar);

        txtTips = (TextView) findViewById(R.id.txtTips);

        btnCandidate = (ImageView) findViewById(R.id.btnCandidate);

        btnCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        btnParty = (ImageView)findViewById(R.id.btnParty);

        btnParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PartyListActivity.class);
                startActivity(intent);
            }
        });

        btnKnowledge = (ImageView)findViewById(R.id.btnKnowledge);
        btnKnowledge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, KnowledgeActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        btnGame =(ImageView) findViewById(R.id.btnGame);
        btnGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GameActivity.class));
            }
        });

        btnSearch = (ImageView) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CheckVoterActivity.class));
            }
        });



        int days = new TimeUtils().daysCount();
        if (days > 0) {
            randomTips();
        }


        imgKnowledge = (ImageView) findViewById(R.id.imgKnowledge);
        imgKnowledge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, KnowledgeActivity.class));
                overridePendingTransition(0, 0);
            }
        });

        imgCandidate = (ImageView) findViewById(R.id.imgCandidate);
        imgCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SearchActivity.class));

            }
        });

        imgParty = (ImageView) findViewById(R.id.imgParty);
        imgParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PartyListActivity.class));
            }
        });

        imgGame = (ImageView) findViewById(R.id.imgGame);
        imgGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GameActivity.class));
            }
        });

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
    }



    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.license:
                showLicenseDialog();
                return true;

            case R.id.about:
                goToAbout();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }



    private void checkShowTutorial() {
        int oldVersionCode = PrefConstants.getAppPrefInt(this, "version_code");
        int currentVersionCode = SAppUtil.getAppVersionCode(this);
        if (currentVersionCode > oldVersionCode) {
            startActivity(new Intent(MainActivity.this, IntroActivity.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            PrefConstants.putAppPrefInt(this, "version_code", currentVersionCode);
        }
    }

    private void randomTips() {

        final int[] tips = {R.string.tip_one, R.string.tip_two, R.string.tip_three, R.string.tip_four,
                R.string.tip_five, R.string.tip_six};

        mHandler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                Random r = new Random();

                int num = r.nextInt(5);
                txtTips.setText(tips[num]);
                mmtext.prepareView(getBaseContext(), txtTips, mmtext.TEXT_UNICODE, true, true);
            }
        };

        Timer tipChangeTimer = new Timer();
        tipChangeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(Update);
            }
        }, 400, 5000);
    }


    private void  showLicenseDialog() {
        View licenseView = LayoutInflater.from(this).inflate(R.layout.license_dialog, null);
        TextView aboutBodyView = (TextView) licenseView.findViewById(R.id.about_body_extras);
        aboutBodyView.setText(Html.fromHtml(getString(R.string.license_body_extras)));
        aboutBodyView.setMovementMethod(new LinkMovementMethod());
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(licenseView)
                .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();

        Button positiveBtn = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mmtext.prepareView(this, positiveBtn, mmtext.TEXT_UNICODE, true, true);
    }

    private void goToAbout() {
        startActivity(new Intent(MainActivity.this, AboutActivity.class));
    }
}