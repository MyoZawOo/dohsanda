package com.dohsanda.dohsanda.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dohsanda.dohsanda.R;

import org.maepaysoh.maepaysohsdk.models.Candidate;

import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by yemyatthu on 8/4/15.
 */
public class CandidateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private List<Candidate> mCandidates;
  private Context mContext;
  private ClickInterface mClickInterface;

  public CandidateAdapter() {
    mCandidates = new ArrayList<>();
  }

  public void setCandidates(List<Candidate> candidates) {
    mCandidates = candidates;
    notifyDataSetChanged();
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    mContext = parent.getContext();
    View view = LayoutInflater.from(mContext).inflate(R.layout.candidate_item_view, parent, false);
    return new CandidateViewHolder(view);
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    Candidate candidate = mCandidates.get(position);

    Glide.with(mContext)
            .load(candidate.getPhotoUrl())
            .fitCenter()
            .into(((CandidateViewHolder) holder).mCanditatePicture);

    ((CandidateViewHolder) holder).mCandidateName.setText(candidate.getName());
    mmtext.prepareView(mContext, ((CandidateViewHolder) holder)
            .mCandidateName, mmtext.TEXT_UNICODE, true, true);

    String label = candidate.getLegislature()
            + "" + mContext.getResources().getString(R.string.representative);
    ((CandidateViewHolder) holder).mCanidateLegislature.setText(label);
    mmtext.prepareView(mContext, ((CandidateViewHolder) holder)
            .mCanidateLegislature, mmtext.TEXT_UNICODE, true, true);

    String partyName = (candidate.getParty() == null) ? candidate.getPartyName() :
            candidate.getParty().getPartyName();
    if (partyName == null || partyName.isEmpty()) {
      partyName = mContext.getString(R.string.label_single_candidate);
    }
            ((CandidateViewHolder) holder).mCandidatePartyName.setText(partyName);
    mmtext.prepareView(mContext, ((CandidateViewHolder) holder).mCandidatePartyName,
            mmtext.TEXT_UNICODE, true, true);
  }

  @Override public int getItemCount() {
    return mCandidates != null ? mCandidates.size() : 0;
  }

  public void setOnItemClickListener(ClickInterface clickInterface) {
    mClickInterface = clickInterface;
  }

  public interface ClickInterface {
    void onItemClick(View view, int position);
  }

  class CandidateViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView mCanditatePicture;
    private TextView mCandidateName;
    private TextView mCanidateLegislature;
    private TextView mCandidatePartyName;

    public CandidateViewHolder(View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      mCanditatePicture = (ImageView) itemView.findViewById(R.id.candidate_picture);
      mCandidateName = (TextView) itemView.findViewById(R.id.candidate_name);
      mCanidateLegislature = (TextView) itemView.findViewById(R.id.candidate_legislature);
      mCandidatePartyName = (TextView) itemView.findViewById(R.id.candidate_party_name);
    }

    @Override public void onClick(View view) {
      if (mClickInterface != null) {
        mClickInterface.onItemClick(view, getAdapterPosition());
      }
    }
  }
}
