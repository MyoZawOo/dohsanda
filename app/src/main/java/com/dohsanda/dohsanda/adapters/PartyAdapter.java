package com.dohsanda.dohsanda.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.dohsanda.dohsanda.R;

import org.maepaysoh.maepaysohsdk.models.Party;

import java.util.ArrayList;
import java.util.List;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by Ye Lin Aung on 15/08/04.
 */
public class PartyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private Context mContext;
  private List<Party> mParties;
  private ClickInterface mClickInterface;

  public PartyAdapter() {
    mParties = new ArrayList<>();
  }

  public void setParties(List<Party> parties) {
    mParties = parties;
    notifyDataSetChanged();
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    mContext = parent.getContext();

    View view = LayoutInflater.from(mContext).inflate(R.layout.item_party, parent, false);
    return new PartyViewHolder(view);
  }

  @Override public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
    Party party = mParties.get(position);

    ((PartyViewHolder)holder).mPartyNameMyanmar.setText(party.getPartyName());
    mmtext.prepareView(mContext, ((PartyViewHolder)holder).mPartyNameMyanmar,
            mmtext.TEXT_UNICODE, true, true);
    ((PartyViewHolder)holder).mPartyNameEnglish.setText(party.getPartyNameEnglish());

    Glide.with(mContext)
        .load(party.getPartyFlag())
        .asBitmap()
        .fitCenter()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(new BitmapImageViewTarget(((PartyViewHolder)holder).mPartyFlag) {
          @Override
          public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            super.onResourceReady(resource, glideAnimation);
          }
        });
  }

  public void setOnItemClickListener(ClickInterface clickInterface) {
    mClickInterface = clickInterface;
  }

  @Override public int getItemCount() {
    return mParties != null ? mParties.size() : 0;
  }

  public interface ClickInterface {
    void onItemClick(View view, int position);
  }

  class PartyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView mPartyNameEnglish;
    private TextView mPartyNameMyanmar;
    private ImageView mPartyFlag;

    public PartyViewHolder(View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      mPartyNameEnglish = (TextView) itemView.findViewById(R.id.party_name_english);
      mPartyNameMyanmar = (TextView) itemView.findViewById(R.id.party_name_myanmar);
      mPartyFlag = (ImageView) itemView.findViewById(R.id.party_flag);
    }

    @Override public void onClick(View view) {
      if (mClickInterface != null) {
        mClickInterface.onItemClick(view, getAdapterPosition());
      }
    }
  }
}
