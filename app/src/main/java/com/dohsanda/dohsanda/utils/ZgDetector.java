package com.dohsanda.dohsanda.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author SH
 */
public class ZgDetector {
    private static final Pattern ZAWGYI_DETECT_PATTERN = Pattern.compile(
            // A regular expression matched if text is Zawgyi encoding.
            "\u1031| ေ[က-အ]်|[က-အ]း"
    );

    private static final Pattern UNICODE_DETECT_PATTERN = Pattern.compile(
            "[ဃငဆဇဈဉညဋဌဍဎဏဒဓနဘရဝဟဠအ]်|ျ[က-အ]ါ|ျ[ါ-း]|\u103e|\u103f|\u1031[^\u1000-\u1021\u103b\u1040\u106a\u106b\u107e-\u1084\u108f\u1090]|\u1031$|\u1031[က-အ]\u1032|\u1025\u102f|\u103c\u103d[\u1000-\u1001]|ည်း|ျင်း|င်|န်း|ျာ|င့်"
    );

    public static boolean isZg(CharSequence input) {
        Matcher matcher = ZAWGYI_DETECT_PATTERN.matcher(input);
        return matcher.find();
    }

    public static boolean isUni(CharSequence input) {
        Matcher matcher = UNICODE_DETECT_PATTERN.matcher(input);
        return matcher.find();
    }
}
