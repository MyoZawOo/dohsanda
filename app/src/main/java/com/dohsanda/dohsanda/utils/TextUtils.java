package com.dohsanda.dohsanda.utils;

import android.content.Context;

import com.dohsanda.dohsanda.R;

/**
 * Created by arkar on 9/25/15.
 */
public class TextUtils {
    private static final String[] ENG_NUM = {
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9"
    };

    private static final String[] MYA_NUM = {
            "၀",
            "၁",
            "၂",
            "၃",
            "၄",
            "၅",
            "၆",
            "၇",
            "၈",
            "၉"
    };

    public static String convertToMM(String engNumeral) {
        int engLen = engNumeral.length();
        int mmLen = MYA_NUM.length;
        boolean isFound = false;
        String result = "";
        for (int i = 0; i < engLen; i++) {
            char temp = engNumeral.charAt(i);
            for (int j = 0; j < mmLen; j++) {
                if (temp == ENG_NUM[j].charAt(0)) {
                    result += MYA_NUM[j];
                    isFound = true;
                }
            }
            if (! isFound) {
                result += temp;
            }
            isFound = false;
        }
        return result;
    }

    public static String checkDataIsProvided(String data, Context context) {
        if (data == null || data.isEmpty()) {
            return context.getString(R.string.label_no_data);
        }
        return data;
    }
}
