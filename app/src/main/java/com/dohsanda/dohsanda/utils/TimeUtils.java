package com.dohsanda.dohsanda.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by arkar on 9/18/15.
 */
public class TimeUtils {

    public static String getDate(long timestamp){
        try{
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date netDate = (new Date(timestamp));
            return sdf.format(netDate);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String getDateFromUnixTimeStamp(long timestamp){
        try{
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(timestamp * 1000);
            return date.get(Calendar.DAY_OF_MONTH) + "/"
                    + (date.get(Calendar.MONTH) + 1) + "/"
                    + date.get(Calendar.YEAR);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String convertISO8601toDateFormat(String iso8601) {
        return (iso8601 == null) ? null : getDate(Long.parseLong(iso8601.replaceAll("T.*?Z", "")));
    }

    public int daysCount() {
        final Calendar c = Calendar.getInstance();
        c.set(2015, Calendar.NOVEMBER, 8);
        final Calendar today = Calendar.getInstance();
        final long millis = c.getTimeInMillis() - today.getTimeInMillis();
        final long days = millis / 86400000; // Convert to days
        return (int)days ;
    }
}
