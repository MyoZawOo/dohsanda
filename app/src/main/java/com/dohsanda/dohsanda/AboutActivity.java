package com.dohsanda.dohsanda;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mm.technomation.tmmtextutilities.mmtext;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView txtMore = (TextView) findViewById(R.id.txt_more);
        txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAboutDialog();
            }
        });

        TextView txtVersion = (TextView) findViewById(R.id.version);
        txtVersion.setText(Constants.VERSION);

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);
    }

    private void  showAboutDialog() {
        View aboutView = LayoutInflater.from(this).inflate(R.layout.about_dialog, null);
        TextView aboutRef = (TextView) aboutView.findViewById(R.id.about_reference);
        TextView aboutDev = (TextView) aboutView.findViewById(R.id.about_developer);
        TextView aboutIllus = (TextView) aboutView.findViewById(R.id.about_illustration);
        TextView aboutContact = (TextView) aboutView.findViewById(R.id.about_contact);
        TextView aboutRefLabel = (TextView) aboutView.findViewById(R.id.about_reference_label);
        TextView aboutApiLabel = (TextView) aboutView.findViewById(R.id.about_api_label);
        TextView aboutApi = (TextView) aboutView.findViewById(R.id.about_api);
        TextView aboutApiLink = (TextView) aboutView.findViewById(R.id.about_api_link);
        TextView aboutVoterGuide = (TextView) aboutView.findViewById(R.id.about_voter_guide);
        TextView aboutVoterGuideLabel = (TextView) aboutView.findViewById(R.id.about_voter_guide_label);
        mmtext.prepareView(this, aboutRef, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, aboutRefLabel, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, aboutApiLabel, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, aboutApi, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, aboutVoterGuide, mmtext.TEXT_UNICODE, true, true);
        mmtext.prepareView(this, aboutVoterGuideLabel, mmtext.TEXT_UNICODE, true, true);
        aboutContact.setMovementMethod(new LinkMovementMethod());
        aboutDev.setMovementMethod(new LinkMovementMethod());
        aboutIllus.setMovementMethod(new LinkMovementMethod());
        aboutApiLink.setMovementMethod(new LinkMovementMethod());
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(aboutView)
                .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
        Button positiveBtn = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mmtext.prepareView(this, positiveBtn, mmtext.TEXT_UNICODE, true, true);
    }


}
