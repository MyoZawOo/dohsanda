package com.dohsanda.dohsanda;

import android.app.Application;
import android.content.Context;

import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;

/**
 * Created by yemyatthu on 8/21/15.
 */
public class MaePaySoh extends Application {
  private static Context sContext;
  private static MaePaySohApiWrapper mMaePaySohApiWrapper;

  public static MaePaySohApiWrapper getMaePaySohWrapper() {
    if (mMaePaySohApiWrapper == null) {
      mMaePaySohApiWrapper = new MaePaySohApiWrapper(sContext);
    }
    return mMaePaySohApiWrapper;
  }

  @Override public void onCreate() {
    super.onCreate();
    MaePaySoh.sContext = getApplicationContext();
    ParseCrashReporting.enable(this);
    Parse.initialize(this, Constants.PARSE_APP_ID, Constants.PARSE_CLIENT_KEY);
    ParseInstallation.getCurrentInstallation().saveInBackground();
  }
}
