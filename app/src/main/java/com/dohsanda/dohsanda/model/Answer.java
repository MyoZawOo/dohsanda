package com.dohsanda.dohsanda.model;

/**
 * Created by arkar on 10/5/15.
 */
public class Answer {
    private String text;

    public Answer(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
