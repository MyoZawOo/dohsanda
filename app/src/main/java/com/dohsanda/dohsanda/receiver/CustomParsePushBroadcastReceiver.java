package com.dohsanda.dohsanda.receiver;

import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

import org.rabbitconverter.rabbit.Rabbit;

/**
 * Created by arkar on 8/25/15.
 */
public class CustomParsePushBroadcastReceiver extends ParsePushBroadcastReceiver {

    private static final String PARSE_EXTRA_DATA_KEY = "com.parse.Data";

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >=
                        android.os.Build.VERSION_CODES.LOLLIPOP) {
            String text = intent.getExtras().getString(PARSE_EXTRA_DATA_KEY) + "";
            text = Rabbit.zg2uni(text);
            intent.removeExtra(PARSE_EXTRA_DATA_KEY);
            intent.putExtra(PARSE_EXTRA_DATA_KEY, text);
        }
        super.onPushReceive(context, intent);
    }


}
