package com.dohsanda.intro;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.dohsanda.dohsanda.MaePaySoh;
import com.dohsanda.dohsanda.R;
import com.dohsanda.education.ViewPagerFragment;
import com.nineoldandroids.view.ViewHelper;

import org.maepaysoh.maepaysohsdk.MaePaySohApiWrapper;
import org.maepaysoh.maepaysohsdk.TownshipAPIHelper;

import mm.technomation.tmmtextutilities.mmtext;

/**
 * Created by myozawoo on 9/21/15.
 */
public class IntroActivity extends AppCompatActivity {

    static final int NUM_PAGES = 4;

    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private LinearLayout circles;
    private ImageButton skip;
    private Button done;
    private ImageButton next;
    private View mTutorialContainer;
    private View mProgressContainer;
    private ProgressBar mProgressBar;
    private boolean isOpaque = true;

    private PopulateTownshipDataTask mPopulateTownshipDataTask;
    private boolean isTaskFinish = false;
    private boolean isTutorialFinish = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.intro_activity);

        mProgressContainer = findViewById(R.id.progress_container);
        mTutorialContainer = findViewById(R.id.tutorial_container);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.primary), PorterDuff.Mode.SRC_ATOP);

        skip = ImageButton.class.cast(findViewById(R.id.skip));
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() - 1, true);
            }
        });
        skip.setVisibility(View.GONE);

        next = ImageButton.class.cast(findViewById(R.id.next));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() + 1, true);
            }
        });
        next.setVisibility(View.VISIBLE);

        done = Button.class.cast(findViewById(R.id.done));
        done.setText("ထွက်မည်");
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTaskFinish) {
                    endTutorial();
                } else {
                    isTutorialFinish = true;
                    mProgressContainer.setVisibility(View.VISIBLE);
                    mTutorialContainer.setVisibility(View.GONE);
                }
            }
        });


        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setPageTransformer(true, new CrossfadePageTransformer());
        //pager.setCurrentItem(2);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == NUM_PAGES - 2 && positionOffset > 0) {
                    if (isOpaque) {
                        pager.setBackgroundColor(Color.TRANSPARENT);
                        isOpaque = false;
                    }
                } else {
                    if (!isOpaque) {
                        pager.setBackgroundColor(getResources().getColor(R.color.primary_material_light));
                        isOpaque = true;
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {


                setIndicator(position);
                if (position == 0) {
                    skip.setVisibility(View.GONE);
                } else if (position == NUM_PAGES - 2) {
                    skip.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                    done.setVisibility(View.VISIBLE);
                } else if (position < NUM_PAGES - 2) {
                    skip.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);
                    done.setVisibility(View.GONE);
                } else if (position == NUM_PAGES - 1) {
                    if (isTaskFinish) {
                        endTutorial();
                    } else {
                        isTutorialFinish = true;
                        mProgressContainer.setVisibility(View.VISIBLE);
                        mTutorialContainer.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        buildCircles();



        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true, true);

        mPopulateTownshipDataTask = new PopulateTownshipDataTask();
        mPopulateTownshipDataTask.execute();
    }

    @Override
    protected void  onPause() {
        super.onPause();
//        if (mPopulateTownshipDataTask != null) {
//            mPopulateTownshipDataTask.cancel(true);
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(pager!=null){
            pager.clearOnPageChangeListeners();
        }
    }

    private void buildCircles(){
        circles = LinearLayout.class.cast(findViewById(R.id.circles));

        float scale = getResources().getDisplayMetrics().density;
        int padding = (int) (5 * scale + 0.5f);

        for(int i = 0 ; i < NUM_PAGES - 1 ; i++){

            ImageView circle = new ImageView(this);

            Bitmap tmpBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.inactive_stick);
            Bitmap circleBitmap = Bitmap.createScaledBitmap(tmpBitmap, (int) scale * 20, (int) scale * 2, false);
            //circle.setImageResource(R.drawable.ic_swipe_indicator_white_18dp);
            circle.setImageBitmap(circleBitmap);
            // circle.setColorFilter(getResources().getColor(R.color.primary));
            circle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            circle.setAdjustViewBounds(true);
            //circle.setPadding(padding, 0, padding, 0);
            circles.addView(circle);
        }
        setIndicator(0);
    }

    private void setIndicator(int index){
        if(index < NUM_PAGES){
            for(int i = 0 ; i < NUM_PAGES - 1 ; i++){
                ImageView circle = (ImageView) circles.getChildAt(i);
                if(i == index){
                    float scale = getResources().getDisplayMetrics().density;
                    Bitmap tmpBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.whtie_stick);
                    Bitmap whiteStick = Bitmap.createScaledBitmap(tmpBitmap, (int)scale*20, (int)scale*2, false);
                    circle.setImageBitmap(whiteStick);

                    //circle.setColorFilter(getResources().getColor(R.color.text_selected));
                }else {
                    float scale = getResources().getDisplayMetrics().density;
                    Bitmap tmpBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.inactive_stick);
                    Bitmap circleBitmap = Bitmap.createScaledBitmap(tmpBitmap, (int)scale*20, (int)scale*2, false);
                    circle.setImageBitmap(circleBitmap);
                    //circle.setColorFilter(getResources().getColor(android.R.color.transparent));
                }
            }
        }
    }


    private void endTutorial(){
        finish();
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    public void onBackPressed() {

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ViewPagerFragment tp = null;
            switch(position){
                case 0:
                    tp = ViewPagerFragment.newInstance(R.layout.intro_fragment_one);
                    break;
                case 1:
                    tp = ViewPagerFragment.newInstance(R.layout.intro_fragment_two);
                    break;
                case 2:
                    tp = ViewPagerFragment.newInstance(R.layout.intro_fragment_three);
                    break;

                case 3:
                    tp = ViewPagerFragment.newInstance(R.layout.infographic_fragment5);
                    break;
            }

            return tp;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class CrossfadePageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View page, float position) {
            int pageWidth = page.getWidth();

            View backgroundView = page.findViewById(R.id.intro_fragment);
            View img = page.findViewById(R.id.img_intro);

//            View text_content = page.findViewById(R.id.txt_intro);

            if(0 <= position && position < 1){
                ViewHelper.setTranslationX(page,pageWidth * -position);
            }
            if(-1 < position && position < 0){
                ViewHelper.setTranslationX(page,pageWidth * -position);
            }

            if(position <= -1.0f || position >= 1.0f) {
            } else if( position == 0.0f ) {
            } else {
                if(backgroundView != null) {
                    ViewHelper.setAlpha(backgroundView,1.0f - Math.abs(position));
                }

                if (img != null) {
                    ViewHelper.setTranslationX(img,pageWidth * position);
                    ViewHelper.setAlpha(img,1.0f - Math.abs(position));
                }
            }
        }
    }

    private class PopulateTownshipDataTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            MaePaySohApiWrapper maePaySohWrapper = MaePaySoh.getMaePaySohWrapper();
            if (! maePaySohWrapper.isSavedTownship()) {
                TownshipAPIHelper townshipAPIHelper = maePaySohWrapper.getTownshipApiHelper();
                townshipAPIHelper.createTownship();
                maePaySohWrapper.setSavedTownship();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (isTutorialFinish) {
                endTutorial();
            } else {
                isTaskFinish = true;
            }
        }
    }
}