package com.dohsanda.intro;

/**
 * Created by myozawoo on 9/21/15.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.dohsanda.dohsanda.MainActivity;
import com.dohsanda.dohsanda.R;
import com.dohsanda.dohsanda.utils.TextUtils;
import com.dohsanda.dohsanda.utils.TimeUtils;

import mm.technomation.tmmtextutilities.mmtext;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        finish();
                    }
                }, 500);
            }
        }, 1500);

        TextView prefix = (TextView) findViewById(R.id.prefix_remainingDays);
        TextView txtDay = (TextView) findViewById(R.id.txt_remainingDays);

        int days = new TimeUtils().daysCount();

        if (days == 0) {
            txtDay.setText("ဒီနေ့ဟာ မဲ ပေးရမယ့် နေ့ဖြစ်ပါတယ်။");
            prefix.setVisibility(View.GONE);
        } else if (days > 0) {
            txtDay.setText( "(" + TextUtils.convertToMM(days + "")  + ") ရက်သာလိုပါတော့သည်");
        } else {
            prefix.setVisibility(View.GONE);
            txtDay.setVisibility(View.GONE);
        }

        mmtext.prepareActivity(this, mmtext.TEXT_UNICODE, true,true);
    }
}
